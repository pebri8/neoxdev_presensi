-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 24, 2018 at 05:31 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `presensi`
--

-- --------------------------------------------------------

--
-- Table structure for table `aktivitas`
--

CREATE TABLE `aktivitas` (
  `id_aktivitas` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `keterangan` varchar(500) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '2018-03-14 23:18:18'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aktivitas`
--

INSERT INTO `aktivitas` (`id_aktivitas`, `id_user`, `keterangan`, `tanggal`) VALUES
(42, 1, 'Mengedit data Administrasipada menu bagian', '2018-03-14 23:18:18'),
(43, 1, 'Mengubah Informasi Pribadi', '2018-03-14 23:20:01'),
(44, 1, 'Mengubah Informasi Pribadi', '2018-03-14 23:21:03'),
(45, 1, 'Edit Kata Sandi', '2018-03-14 23:21:18'),
(46, 1, 'Menambah data Yudha pada menu karyawan', '2018-03-15 19:01:51'),
(47, 1, 'Reset password pada karyawan Hanif Alfian', '2018-03-15 20:03:38'),
(48, 1, 'Reset password pada karyawan Nisa', '2018-03-15 20:04:44'),
(49, 1, 'Reset device id pada karyawan Pebri Antara', '2018-03-14 23:18:18'),
(50, 1, 'Reset device id pada karyawan Pendi Ventri Hendika', '2018-03-14 23:18:18'),
(51, 1, 'Reset password pada karyawan Pebri Antara', '2018-03-14 23:18:18'),
(52, 1, 'Reset password pada karyawan Yudha', '2018-03-14 23:18:18'),
(53, 1, 'Reset device id pada karyawan Yudha', '2018-03-14 23:18:18'),
(54, 1, 'Reset password pada karyawan Yudha', '2018-03-14 23:18:18'),
(55, 1, 'Reset password pada karyawan Lily Handayani', '2018-03-14 23:18:18'),
(56, 1, 'Reset device id pada karyawan Lily Handayani', '2018-03-14 23:18:18'),
(57, 1, 'Reset password pada karyawan Lily Handayani', '2018-03-14 23:18:18'),
(58, 1, 'Reset device id pada karyawan Lily Handayani', '2018-03-14 23:18:18'),
(59, 1, 'Reset device id pada karyawan Lily Handayani', '2018-03-14 23:18:18'),
(60, 1, 'Reset password pada karyawan Lily Handayani', '2018-03-14 23:18:18'),
(61, 1, 'Reset device id pada karyawan Lily Handayani', '2018-03-14 23:18:18'),
(62, 1, 'Reset password pada karyawan Lily Handayani', '2018-03-14 23:18:18'),
(63, 1, 'Edit Kata Sandi', '2018-03-14 23:18:18'),
(64, 1, 'Edit Kata Sandi', '2018-03-14 23:18:18'),
(65, 1, 'Edit Kata Sandi', '2018-03-14 23:18:18'),
(66, 1, 'Menambah data Kontenpada menu bagian', '2018-03-14 23:18:18'),
(67, 1, 'Menambah data Hanif Aisyah pada menu karyawan', '2018-03-14 23:18:18'),
(68, 1, 'Menambah data Anita pada menu karyawan', '2018-03-14 23:18:18'),
(69, 1, 'Menambah data Ayung pada menu karyawan', '2018-03-14 23:18:18'),
(70, 1, 'Reset password pada karyawan Anita', '2018-03-14 23:18:18'),
(71, 1, 'Reset device id pada karyawan Anita', '2018-03-14 23:18:18'),
(72, 1, 'Reset password pada karyawan Hanif Aisyah', '2018-03-14 23:18:18'),
(73, 1, 'Reset password pada karyawan Ayung', '2018-03-14 23:18:18'),
(74, 1, 'Menambah data Maya pada menu karyawan', '2018-03-14 23:18:18'),
(75, 1, 'Reset password pada karyawan Maya', '2018-03-14 23:18:18'),
(76, 1, 'Reset password pada karyawan Maya', '2018-03-14 23:18:18'),
(77, 1, 'Reset device id pada karyawan Maya', '2018-03-14 23:18:18'),
(78, 1, 'Reset device id pada karyawan Maya', '2018-03-14 23:18:18'),
(79, 1, 'Reset device id pada karyawan Anita', '2018-03-14 23:18:18'),
(80, 1, 'Reset password pada karyawan Anita', '2018-03-14 23:18:18'),
(81, 1, 'Reset password pada karyawan Anita', '2018-03-14 23:18:18'),
(82, 1, 'Reset device id pada karyawan Anita', '2018-03-14 23:18:18'),
(83, 1, 'Reset password pada karyawan Maya', '2018-03-14 23:18:18'),
(84, 1, 'Reset password pada karyawan Maya', '2018-03-14 23:18:18'),
(85, 25, 'Reset device id pada karyawan Admin Check', '2018-03-14 23:18:18'),
(86, 25, 'Reset password pada karyawan Admin Check', '2018-03-14 23:18:18'),
(87, 25, 'Reset device id pada karyawan Admin Check', '2018-03-14 23:18:18'),
(88, 25, 'Menambah data Wahyu pada menu karyawan', '2018-03-14 23:18:18'),
(89, 25, 'Menghapus data Wahyu pada menu karyawan', '2018-03-14 23:18:18');

-- --------------------------------------------------------

--
-- Table structure for table `bagian`
--

CREATE TABLE `bagian` (
  `id_bagian` int(11) NOT NULL,
  `nama_bagian` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bagian`
--

INSERT INTO `bagian` (`id_bagian`, `nama_bagian`) VALUES
(1, 'Administrasi'),
(7, 'Video'),
(8, 'Tech Web'),
(9, 'Tech Android'),
(10, 'C E O'),
(11, 'Konten');

-- --------------------------------------------------------

--
-- Table structure for table `ip`
--

CREATE TABLE `ip` (
  `id_ip` int(11) NOT NULL,
  `ip_address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ip`
--

INSERT INTO `ip` (`id_ip`, `ip_address`) VALUES
(1, '1'),
(2, '36.72.218.34'),
(5, '36.72.218.82'),
(6, '114.142.171.17'),
(7, '219.83.68.69'),
(8, '36.80.158.108'),
(9, '36.73.16.135'),
(10, '180.254.20.62'),
(11, '180.246.91.140'),
(12, '36.72.217.105'),
(13, '36.80.152.211'),
(14, '36.73.142.218'),
(15, '36.73.155.46'),
(16, '36.80.180.117'),
(17, '36.73.157.164'),
(18, '36.79.61.200'),
(19, '180.254.29.151'),
(20, '36.79.18.209');

-- --------------------------------------------------------

--
-- Table structure for table `izin`
--

CREATE TABLE `izin` (
  `id_izin` int(11) NOT NULL,
  `nama_izin` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `izin`
--

INSERT INTO `izin` (`id_izin`, `nama_izin`) VALUES
(1, 'Super'),
(2, 'Admin'),
(3, 'Pegawai');

-- --------------------------------------------------------

--
-- Table structure for table `presensi`
--

CREATE TABLE `presensi` (
  `id_presensi` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `masuk` datetime NOT NULL,
  `pulang` datetime NOT NULL,
  `status_presensi` varchar(225) NOT NULL COMMENT '0=jam pulang kosong,1=complete ',
  `keterangan_masuk` text,
  `keterangan_pulang` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `presensi`
--

INSERT INTO `presensi` (`id_presensi`, `id_user`, `masuk`, `pulang`, `status_presensi`, `keterangan_masuk`, `keterangan_pulang`) VALUES
(43, 1, '2018-03-14 22:11:19', '2018-03-14 22:11:23', '1', '', ''),
(44, 19, '2018-03-14 09:26:00', '2018-03-14 23:38:57', '1', '', ''),
(45, 19, '2018-03-15 18:53:21', '2018-03-15 23:26:00', '1', '', ''),
(46, 17, '2018-03-15 18:54:37', '2018-03-15 21:06:09', '1', '', ''),
(47, 18, '2018-03-15 18:57:26', '2018-03-15 23:26:00', '1', '', ''),
(48, 15, '2018-03-15 20:03:53', '2018-03-15 22:42:32', '1', '', ''),
(49, 16, '2018-03-15 20:05:14', '2018-03-15 23:04:54', '1', '', ''),
(50, 20, '2018-03-15 21:09:35', '2018-03-15 22:31:24', '1', '', ''),
(51, 17, '2018-03-16 15:55:55', '2018-03-16 22:06:23', '1', '', ''),
(52, 16, '2018-03-16 15:57:21', '2018-03-16 22:25:00', '1', '', ''),
(53, 19, '2018-03-16 17:55:30', '2018-03-16 22:28:28', '1', '', ''),
(54, 20, '2018-03-16 19:12:30', '2018-03-16 22:28:12', '1', '', ''),
(55, 15, '2018-03-16 20:31:41', '2018-03-16 22:25:00', '1', '', ''),
(56, 19, '2018-03-20 18:04:34', '2018-03-20 23:59:00', '1', '', ''),
(57, 19, '2018-03-19 18:06:00', '2018-03-19 23:56:00', '1', '', ''),
(58, 15, '2018-03-20 20:50:01', '2018-03-20 23:20:32', '1', '', ''),
(59, 16, '2018-03-20 19:21:00', '2018-03-20 23:20:39', '1', '', ''),
(60, 17, '2018-03-20 23:20:57', '2018-03-20 23:20:57', '0', '', ''),
(61, 19, '2018-04-05 18:00:00', '2018-04-05 22:06:44', '1', '', ''),
(62, 20, '2018-04-03 19:15:00', '2018-04-03 23:10:00', '1', '', ''),
(63, 16, '2018-04-03 19:31:00', '2018-04-03 22:37:00', '1', '', ''),
(64, 15, '2018-04-04 20:15:00', '2018-04-04 22:04:00', '1', '', ''),
(66, 18, '2018-04-02 14:12:00', '2018-04-02 17:42:00', '1', '', ''),
(67, 18, '2018-04-03 13:09:00', '2018-04-05 23:04:00', '1', '', ''),
(68, 18, '2018-04-04 10:29:00', '2018-04-04 15:34:00', '1', '', ''),
(69, 16, '2018-04-05 19:45:00', '2018-04-05 23:20:00', '1', '', ''),
(71, 15, '2018-04-05 19:56:00', '2018-04-06 00:02:00', '1', '', ''),
(72, 17, '2018-04-05 19:46:00', '2018-04-06 00:04:00', '1', '', ''),
(73, 18, '2018-04-05 18:43:00', '2018-04-06 00:07:00', '1', '', ''),
(74, 20, '2018-04-05 18:38:00', '2018-04-06 00:18:00', '1', '', ''),
(75, 15, '2018-04-06 16:51:50', '2018-04-06 21:14:15', '1', '', ''),
(76, 19, '2018-04-06 17:16:10', '2018-04-06 17:16:10', '0', '', ''),
(77, 15, '2018-04-07 18:40:17', '2018-04-07 22:19:30', '1', '', ''),
(78, 16, '2018-04-07 19:56:51', '2018-04-07 22:31:19', '1', '', ''),
(79, 17, '2018-04-07 20:16:33', '2018-04-07 23:11:23', '1', '', 'Selesai mengerjakan input keterangan presensi'),
(80, 15, '2018-04-09 13:32:13', '2018-04-09 18:32:14', '1', 'edit konten audio\r\nmanata take video profil', 'edit konten dengerin\r\nnata rencana take video profil'),
(81, 20, '2018-04-09 14:09:41', '2018-04-09 20:32:49', '1', 'Menu search navigasi, ganti warna text nav drawer, fixing lihat yang lain di home fragment', 'Fungsi search nav drawer, fix menu item school, change color menu drawer'),
(82, 17, '2018-04-09 15:32:40', '2018-04-09 20:58:51', '1', 'Membuat database presensi diluar', 'Ubah icon di materi selesai. Create thumbnail video dikit lagi'),
(84, 18, '2018-04-09 18:18:10', '2018-04-09 21:04:31', '1', 'Print rangking', 'Print rangking done, update keuangan, cek konten teks fitra april'),
(85, 15, '2018-04-10 13:49:38', '2018-04-10 19:39:00', '1', 'take video profil', 'Take video profil'),
(86, 16, '2018-04-10 18:42:08', '2018-04-10 22:33:43', '1', 'Membagi per part materi pkn kelas 7 smp 15\r\n', 'Membagi part materi pkn kelas 7 snp 15\r\nRewrite 12-13 Feb'),
(87, 18, '2018-04-10 18:49:57', '2018-04-10 23:59:00', '1', 'Cek konten', ''),
(88, 20, '2018-04-10 19:14:35', '2018-04-10 23:59:00', '1', 'Fix lihat yang lain pada home fragment', '1. Fixing Lihat Selanjutnya di home fragment\r\n2. Fixing bug menu item saat Dari menu search ke menu home\r\n3. Fixing icon navdrawer jade biri saat posisi checked\r\n4. Fixing scroll test'),
(89, 17, '2018-04-10 19:55:36', '2018-04-10 22:29:01', '1', 'Merubah thumbnail kuis', 'Ganti thumbnail quiz'),
(90, 20, '2018-04-11 09:21:45', '2018-04-11 13:45:11', '1', 'Fixing video player', 'Masih fixing player video'),
(91, 17, '2018-04-11 16:12:00', '2018-04-11 21:17:00', '1', 'Membenahi icon user\r\nkoordinasi tim dev dan video\r\nmembayar internet\r\ntesting dengerin Web', 'Membenahi icon user\r\nkoordinasi tim dev dan video\r\nmembayar internet\r\ntesting dengerin Web'),
(92, 15, '2018-04-11 13:53:33', '2018-04-11 18:40:00', '1', 'edit konten audio', 'Edit konten audio'),
(93, 18, '2018-04-11 14:02:36', '2018-04-11 22:38:00', '1', 'Membeli meja kursi, buat surat perjanjian untuk input data capil, cek konten ', ''),
(94, 16, '2018-04-11 18:41:35', '2018-04-11 22:20:00', '1', 'Rewrite 13 Februari ', 'Recording Materi PKN SMPN 15'),
(95, 15, '2018-04-12 13:37:47', '2018-04-12 18:46:45', '1', 'edit konten audio', 'edit konten audio'),
(96, 19, '2018-04-12 14:29:26', '2018-04-12 23:59:00', '1', 'Menerima karyawan\r\nSetting server', ''),
(97, 21, '2018-04-12 15:17:01', '2018-04-12 15:17:37', '1', '  2001-2050/CSA.1920/U/1989 (selesai) \r\n  4801-4850/CSA.1920/t/1999 (selesai)', '  2001-2050/CSA.1920/U/1989 (selesai)\r\n  4801-4850/CSA.1920/t/1999 (selesai)'),
(98, 20, '2018-04-12 19:43:16', '2018-04-12 23:53:17', '1', 'Riset video player android', 'Fixing video saat di buka pertama langsung muncul videonya, fixing ratio video saat pertama kali dibuka, fix bug saat di back dari album video ke menu home'),
(99, 21, '2018-04-13 16:11:25', '2018-04-13 22:16:00', '1', '201-250/csa.1920/u/1995 sampai 851-900/csa.1920/t/1995', ''),
(100, 23, '2018-04-13 19:13:05', '2018-04-16 23:58:00', '1', 'input data', 'input data'),
(101, 22, '2018-04-13 19:13:08', '2018-04-13 19:13:08', '1', '0', '12501-2550/CSA.1920/U/1997 \r\n2 2551-2600/CSA.1920/U/1997 \r\n3 2651-2700/CSA.1920/U/1997 \r\n4 2801-2850/CSA.1920/U/1997 \r\n5 4101-4150/CSA.1920/T/1997 \r\n6 4151-4200/CSA.1920/T/1997 \r\n7 4251-4300/CSA.1920/T/1997 \r\n8 4301-4350/CSA.1920/T/1997 \r\n9 4351-4400/CSA.1920/T/1997 \r\n10 4401-4450/CSA.1920/T/1997 \r\n11 4501-4550/CSA.1920/T/1997 \r\n12 4551-4600/CSA.1920/T/1997 \r\n13 4601-4650/CSA.1920/T/1997 \r\n14 4651-4700/CSA.1920/T/1997 \r\n15 4701-1750/CSA.1920/T/1997'),
(102, 17, '2018-04-13 19:30:45', '2018-04-13 23:55:20', '1', 'Thumnbail playlist', 'Ganti thumbnail user. thumbnail video'),
(103, 18, '2018-04-13 19:33:00', '2018-04-13 23:59:00', '1', 'Cek konten', ''),
(104, 15, '2018-04-13 19:38:10', '2018-04-13 21:35:59', '1', 'edit konten audio', 'edit konten audio'),
(105, 20, '2018-04-13 19:45:04', '2018-04-13 23:44:00', '1', 'Fixing bug', 'Fix bug back pada school materi, fix scrool pada album music, fix menu item pada school materi saat di klik'),
(106, 16, '2018-04-13 19:59:50', '2018-04-13 23:55:38', '1', 'Membagi part materi IPS  Smp 15', 'Rewrite  materi IPS smp 15 sampai poin B\r\n'),
(107, 24, '2018-04-13 20:12:00', '2018-04-13 22:17:00', '1', '0', ''),
(108, 23, '2018-04-14 10:29:33', '2018-04-14 19:46:10', '1', '0', '9 bendel\r\n'),
(109, 22, '2018-04-14 10:40:34', '2018-04-14 18:11:17', '1', '0', '15 bundel, masih kurang 5 bundel'),
(110, 21, '2018-04-14 11:23:28', '2018-04-14 22:15:00', '1', '0', ''),
(111, 16, '2018-04-14 14:04:04', '2018-04-14 20:51:01', '1', 'Rewrite materi IPS kelas 7 smp 15 bagian B ', 'Rewrite & membagi part materi ips kelas 7 smp 15\r\n'),
(112, 20, '2018-04-14 15:32:02', '2018-04-14 20:52:54', '1', 'Fixing judul yang terlalu panjang', 'Fixing title yang terlalu panjang, masih fixing masalah music di click ganti warna text'),
(113, 24, '2018-04-14 18:10:28', '2018-04-14 19:38:03', '1', '0', '27 BENDEL'),
(114, 20, '2018-04-16 14:49:17', '2018-04-16 18:27:00', '1', 'Fixing auto next song', 'Player album (play all)\r\nRiset active play file'),
(115, 15, '2018-04-16 15:06:25', '2018-04-16 19:58:00', '1', 'Edit konten audio', 'Editing audio konten'),
(116, 24, '2018-04-16 16:21:37', '2018-04-16 21:14:00', '1', '0', ''),
(117, 23, '2018-04-16 17:30:00', '2018-04-16 22:34:00', '1', 'input data', 'input data'),
(118, 21, '2018-04-16 19:46:00', '2018-04-16 23:13:00', '1', 'input data ', 'input data'),
(119, 22, '2018-04-16 19:32:00', '2018-04-16 23:16:00', '1', 'input data 5 folder', 'input data'),
(120, 15, '2018-04-17 14:18:01', '2018-04-17 20:13:00', '1', 'Edit konten audio', 'Istall audio software\r\nedit audio'),
(121, 20, '2018-04-17 18:49:40', '2018-04-17 23:59:00', '1', 'Update icon subject by server, fixing detail audio', '1. Membuat icon yang ada pada android sensual dengan yang ada pada server\r\n2. Fixing detail album agar di klik ganti Warna (Belum Bisa)\r\n3. Fixing Player setelah pull Dari gitlab (beberapa ada yang berubah setelah pull)'),
(122, 22, '2018-04-17 19:10:03', '2018-04-17 23:59:00', '1', '0', ''),
(123, 23, '2018-04-17 19:10:41', '2018-04-17 23:59:00', '1', '0', ''),
(124, 21, '2018-04-17 19:19:36', '2018-04-17 23:59:00', '1', '843/CSA.1920/T/1997 sampai selesai 1000 data', ''),
(125, 17, '2018-04-17 19:40:09', '2018-04-17 23:59:00', '1', 'Smtp email vps', ''),
(126, 16, '2018-04-17 20:05:49', '2018-04-17 23:48:54', '1', 'Rewrite ips kelas 8 smp 15 \r\n', 'Bagi part bab IV ips kelas 8  smp 15'),
(127, 24, '2018-04-18 09:52:58', '2018-04-18 11:51:43', '1', '0\r\n', '01-50/CSA.1920/T/1995\r\n01-50/CSA.193/R/1990\r\n01-50/CSA.1933/T/1995\r\n101-150/CSA.1920/T/1995\r\n101-150/CSA.1933/T/1995\r\n1101-1150/CSA.1920/U/1998\r\n1151-1200/CSA.1920/U/1992\r\n1251-1300/CSA.1920/U/1992\r\n151-200/CSA.1920/T/1995'),
(128, 20, '2018-04-18 10:31:57', '2018-04-18 14:21:20', '1', 'Riset reset color text recycler view', 'Penambahan navigasi pada test/ujian'),
(129, 15, '2018-04-18 16:25:39', '2018-04-18 20:54:23', '1', 'Edit konten audio', 'Edit konten dengerin'),
(130, 17, '2018-04-18 19:10:40', '2018-04-18 23:10:00', '1', 'Smtp email notifikasi dengerin', ''),
(131, 16, '2018-04-18 19:23:09', '2018-04-18 22:54:04', '1', 'Rewrite 15 Februari', 'Rewrite 15 Feb '),
(132, 23, '2018-04-19 10:48:46', '2018-04-19 17:51:20', '1', '0', '01-50/CSA.1933/U/1997\r\n101-150/CSA.1933/U/1997\r\n12251-12300/CSA.1920/T/1988\r\n151-200/CSA.1920/T/1997\r\n1901-1950/CSA.1920/T/1995\r\n201-250/CSA.1920/T/1997\r\n2201-2250/CSA.1920/U/1996\r\n2251-2300/CSA.1920/T/1995\r\n2251-2300/CSA.1920/U/1996\r\n2301-2350/CSA.1920/T/1995\r\n2351-2400/CSA.1920/T/1995\r\n2351-2400/CSA.1920/U/1996\r\n2401-2450/CSA.1920/T/1995\r\n2401-2450/CSA.1920/U/1996'),
(133, 22, '2018-04-19 14:46:55', '2018-04-19 19:59:06', '1', '0', '3851-3900/ACS.1920/T/1987_B-48-?\r\n3851-3900/CSA.1920/T/1987_A-50\r\n3851-3900/CSA.1920/T/1987_C-49\r\n3901-3950/CSA.1920/T/1987-48\r\n6201-6250/CSA.1920/T/1997-34\r\n6851-6900/CSA.1920/T/1997-43\r\n6601-6650/CSA.1920/T/1997-43\r\n6551-6600/CSA.1920/T/1997-50\r\n6501-6550/CSA.1920/T/1997-48\r\n6401-6450/CSA.1920/T/1997-48\r\n6351-6400/CSA.1920/T/1997-48\r\n6301-6350/CSA.1920/T/1997-49\r\n6251-6300/CSA.1920/T/1997-46\r\n51-100/CSA.1920/U/1997-49\r\n351-400/CSA.1920/R/1997-50\r\n'),
(134, 17, '2018-04-19 16:40:48', '2018-04-19 23:40:29', '1', 'Smtp email dengerin.id', 'Menambahkan dropdown kategori  bidang studi. Ganti gender pada tes. Ganti img default profil'),
(135, 21, '2018-04-19 12:03:00', '2018-04-19 18:03:00', '1', '0', ''),
(136, 24, '2018-04-19 18:35:56', '2018-04-19 18:35:56', '0', '0', NULL),
(137, 20, '2018-04-19 19:17:38', '2018-04-19 23:36:05', '1', 'Fix navigasi quiz', 'Otw membuat video bisa fullscreen'),
(138, 15, '2018-04-19 19:23:48', '2018-04-19 23:37:30', '1', 'Persiapan take\r\nEdit konten audio', 'Edit konten audio\r\n• 17 Maret 18 (Tami) - Mixdown\r\nPersiapan take video profil'),
(139, 16, '2018-04-19 17:00:00', '2018-04-19 21:53:00', '1', '0', 'Kamis 19 April 2018\r\nJam \r\n 17.00 - 17.42 - rewrite 5 konten 16 Feb (selesai)\r\n 18.00 - 18.17 - rewrite 1 konten 17 Feb (selesai)\r\n 18.21 - 19.00 - rewrite 3 konten 17 Feb (selesai)\r\n 19.00 - 19.16 - rewrite 2 konten 18 Feb (selesai)\r\n 20.00 - 20.50 - rewrite 4 konten 18 Feb (selesai)\r\n 21.18 - 21.53 - rewrite 5 konten 19 Feb (selesai)'),
(140, 22, '2018-04-20 08:35:01', '2018-04-20 13:50:08', '1', '0', '1051-1100/CSA.1920/U/1988-50\r\n\r\n1-50/CSA.1917/U/1996-3\r\n\r\n01-50/CSA.1920/R/1996-47\r\n\r\n1-50/CSA.1920/U/1996-50\r\n1-50/CSA.1933/R/1997-50\r\n01-50/CSA.1933/R/1996-50\r\n\r\n951-1000/CSA.1920/T/1993-26\r\n7251-7300/CSA.1920/T/1997-32\r\n\r\n901-950/CSA.1920/T/1993-49\r\n\r\n101-150/CSA.1933/R/1997_A-46\r\n\r\n1-50/CSA.1933/U/1996-50\r\n\r\n1001-1050/CSA.1920/T/1993-49\r\n'),
(141, 15, '2018-04-20 12:58:54', '2018-04-20 15:57:36', '1', 'Take video profil', NULL),
(142, 23, '2018-04-20 14:02:34', '2018-04-20 19:55:17', '1', '0', '2401-2450/CSA.1920/U/1996\r\n2451-2500/CSA.1920/U/1997\r\n251-300/CSA.1933/U/1997\r\n501-550/CSA.1920/T/1987\r\n01-50/CSA.1920/U/1997\r\n101-150/CSA.1920/U/1997\r\n151-200/CSA.1920/U/1992\r\n151-200/CSA.1920/U/1997\r\n11001-11050/CSA.1920/T/1988\r\n1051-1100/CSA.1920/U/1998\r\n01-50/CSA.1920/U/1998_A\r\n01-50/CSA.1920/U/1998_B'),
(143, 24, '2018-04-20 18:54:45', '2018-04-20 18:54:45', '0', '0\r\n', NULL),
(144, 16, '2018-04-20 19:35:07', '2018-04-20 23:13:34', '1', 'Rewrite 20 Feb (on progres)', ' 19.35 - 19.54 - rewrite 1 konten 20 Feb (selesai)\r\n   rewrite 1 konten 18 Feb (selesai)\r\n   pindah  3 konten 18 Feb -> 20 Feb (selesai)\r\n   note : 20 Feb 4 konten = 18 Februaiii 4 konten\r\n 19.55 - 20.30 - rewrite 5 konten 21 Feb (selesai)\r\n 20.35 - 21.08 - rewrite 5 konten 22 Feb (selesai)\r\n 21.09 - 21.40 - rewrite 5 konten 23 Feb (selesai)\r\n 21.42 - 21.45 - rewrite 1 konten 24 Feb (selesai)\r\n 22.04 - 22.27 - rewrite 3 konten 24 Feb (selesai)\r\n   note : 24 Feb 1 konten sama \r\n 22.32 - 23.00 - rewrite 5 konten 25 Feb (selesai)'),
(145, 21, '2018-04-20 19:35:45', '2018-04-20 23:06:54', '1', '01-50/CSA.1920/R/1990 sampai   15451-15500/CSA.1920/T/1988', '01-50/CSA.1920/R/1990\r\n101-150/CSA.1920/R/1990\r\n1351-1400/CSA.1920/T/1990\r\n1451-1500/CSA.1920/T/1990\r\n1451-1500/CSA.1920/T/1995\r\n1501-1550/CSA.1920/T/1990\r\n1501-1550/CSA.1920/T/1995\r\n15401-15401/CSA.1920/T/1988\r\n15451-15500/CSA.1920/T/1988\r\n15501-15550/CSA.1920/T/1988'),
(146, 17, '2018-04-20 20:05:25', '2018-04-20 23:54:44', '1', 'Mengkondisikan frontend', 'Edit datepicker frontend guru'),
(147, 21, '2018-04-21 11:58:16', '2018-04-21 15:15:54', '1', '  1551-1600/CSA.1920/T/1990\r\n  1551-1600/CSA.1920/T/1995\r\n  15551-15600/CSA.1920/T/1995', '1551-1600/CSA.1920/T/1990\r\n1551-1600/CSA.1920/T/1995\r\n15551-15600/CSA.1920/T/1995\r\n15601-15650/CSA.1920/T/1988\r\n15651-15700/CSA.1920/T/1988\r\n15701-15750/CSA.1920/T/1988'),
(148, 16, '2018-04-21 12:03:11', '2018-04-21 21:58:22', '1', 'Rewrite 26 Feb ( on progress)', '12.04 - 12.54 - rewrite 1 konten 25 Feb (selesai)\r\n   rewrite 6 konten 26 Feb (selesai)\r\n 13.10 - 13.37 - rewrite 4 konten 27 Feb (selesai)\r\n   note :  1 konten 27 Feb = tdk di rewrite\r\n 13.40 - 14.04 - rewrite 3 konten 28 Feb (selesai)\r\n   note :  2 konten 28 Feb = tdk di rewrite\r\n\r\nsetelah jam 20.00\r\nanimasi logo dengerin (selesai)\r\nedit animasi -> vidro profile &#40; on progress&#41;\r\n'),
(149, 20, '2018-04-21 12:03:46', '2018-04-21 17:52:00', '1', 'Fullscreen video', 'Fullscreen video'),
(150, 15, '2018-04-21 13:58:00', '2018-04-21 19:27:35', '1', 'Edit profile video', 'Edit video profil\r\n• Cutting + Grading (selesai)\r\n• Kurang opening closing, render'),
(151, 22, '2018-04-21 14:57:20', '2018-04-21 14:57:20', '0', '0', NULL),
(152, 23, '2018-04-21 16:10:00', '2018-04-21 19:19:20', '1', '0', '1001-1050/CSA.1920/T/1999\r\n1001-1050/CSA.1920/U/1998\r\n101-150/CSA.1920/U/1998\r\n11101-11150/CSA.1920/T/1988_A\r\n11151-11200/CSA.1920/T/1998\r\n11201-11250/CSA.1920/T/1988'),
(153, 17, '2018-04-21 21:37:11', '2018-04-22 00:34:00', '1', 'Menampilkan grade smp sma', 'Ganti grade, ganti icon'),
(154, 24, '2018-04-21 21:58:56', '2018-04-21 21:59:44', '1', '0\r\n', '5351-5400/CSA.1920/T/1997\r\n5401-5450/CSA.1920/T/1997\r\n5501-5550/CSA.1920/T/1997_A\r\n551-600/CSA.1920/R/1998\r\n551-600/CSA.1920/U/1992\r\n551-600/CSA.1920/U/1998\r\n5551-5600/CSA.1920/T/1997\r\n5601-5650/CSA.1920/T/1997\r\n5651-5700/CSA.1920/T/1997\r\n5701-5750/CSA.1920/T/1997_A\r\n5751-5800/CSA.1920/T/1997'),
(155, 15, '2018-04-23 10:46:03', '2018-04-23 15:59:09', '1', 'Render profile video\r\nEdit konten dengerin', 'Edit konten audio\r\n• 18 Maret 18 (Tami) - sudah di mixdown\r\nEdit video profil\r\n• Sudah dirender'),
(156, 20, '2018-04-23 15:41:09', '2018-04-23 21:00:15', '1', 'Fullscreen dan atau absensi', 'Masih riset fullscrenn dan presensi'),
(157, 24, '2018-04-23 16:38:54', '2018-04-23 16:38:54', '0', '0\r\n', NULL),
(158, 23, '2018-04-23 18:22:34', '2018-04-23 18:22:34', '0', '0', NULL),
(159, 22, '2018-04-23 18:58:24', '2018-04-23 18:58:24', '0', '0', NULL),
(160, 21, '2018-04-23 20:30:53', '2018-04-23 20:30:53', '0', ' 15751-15800/CSA.1920/T/1988 sampai 1651-1700/CSA.1920/T/1995', NULL),
(164, 25, '2018-04-23 22:05:55', '2018-04-23 22:06:16', '1', 'test1', 'ini dan itu'),
(165, 25, '2018-04-23 22:16:55', '2018-04-23 22:19:18', '1', 'ini presensi kedua', 'kjkjkj'),
(179, 25, '2018-04-24 20:44:58', '2018-04-24 21:14:23', '1', 'asd', 'asdasd');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `id_bagian` varchar(225) NOT NULL,
  `id_izin` int(11) NOT NULL,
  `nama` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL,
  `gambar` text NOT NULL,
  `device_id` text NOT NULL,
  `status_login` int(11) NOT NULL DEFAULT '0' COMMENT '0=blm pernah login,1=sudah pernah login'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `id_bagian`, `id_izin`, `nama`, `email`, `password`, `gambar`, `device_id`, `status_login`) VALUES
(1, '1', 1, 'Administrator', 'admin@neoxdev.com', 'ea9f2a13028b7b9988ee91f7fbf468e5', '\"bill_gates.png\"', '93eb895010c5ff3883e9699353e5c326c9e5db97', 1),
(15, '7', 3, 'Hanif Alfian', '11hanifalfian@gmail.com', '6e756c9f5ea4020fb2264282ae1e21bb', '\"photo_2018-03-14_18-03-44.jpg\"', '3da66863da9b2812ebfde7c2c15bf72783c07aa1', 1),
(16, '7', 3, 'Nisa', 'annisa.mu@students.amikom.ac.id', '9cf81d8026a9018052c429cc4e56739b', '\"Yogyakarta.jpg\"', '6919a0f2d66699aecc5fcc453ac901f0aaf1b6c7', 1),
(17, '8', 3, 'Pebri Antara', 'pebriantara8@gmail.com', '721002e45ce8fc58e4bd08b19b89a38c', '\"pebri.jpg\"', 'dfcc93772db99003e541474f6c8f7214f2760e80', 1),
(18, '1', 1, 'Lily Handayani', 'estherlily03@gmail.com', 'c9e1074f5b3f9fc8ea15d152add07294', '\"lily.jpg\"', 'ef31ad9bccbd46d39254b9c0756a648a4914d934', 1),
(19, '10', 3, 'Pendi Ventri Hendika', 'ven3day@gmail.com', '37f0e884fbad9667e38940169d0a3c95', '\"Tugu_Jogja_istimewa_feeltrip_wisata_edukasi_masa_kini.jpg\"', 'ff53f5bce75b39855835539675ba9da996ccd249', 1),
(20, '9', 3, 'Yudha', 'yudhapratama539@gmail.com', '9a82560ab3f36db8d28241136bad21cc', '\"myprofile_image.JPG\"', '77125ec5fa6982e6a2ccc358870b3e3f1d3b90b4', 1),
(21, '11', 3, 'Hanif Aisyah', 'Hanifaisyah97@gmail.com', '348aabcfdf0004c41e3e199c5bae43e3', '\"hanif_aisyah.png\"', '00d390a503220e7761ff64e28fb8f6eae12a259e', 1),
(22, '11', 3, 'Anita', 'anitanursari@gmail.com', 'ea01e5fd8e4d8832825acdd20eac5104', '\"hanif_aisyah1.png\"', '8cdad0f4f11a17951bf8bce51154de837d0eaed2', 1),
(23, '11', 3, 'Ayung', 'ramadhantiayung@gmail.com', '956350bfd1852bdd1112e1dcfbfae9ee', '\"ayung.png\"', '33e743f197fec2c67332927a7ff2741e81f1ce95', 1),
(24, '11', 3, 'Maya', 'maya.9412@students.amikom.ac.id', '02c43aeb26e1db88c47c4c6cf8001144', '\"hanif_aisyah2.png\"', 'c5cb473ee762ffc0d59d746eaf69eb0a7321677a', 1),
(25, '1', 1, 'Admin Check', 'admin@admin.com', '6ad68256701732811620ff11056a99be', '\"luna-maya-41.jpg\"', '10c3ddee5987fa0d694df22ca595862901a3d1a7', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aktivitas`
--
ALTER TABLE `aktivitas`
  ADD PRIMARY KEY (`id_aktivitas`);

--
-- Indexes for table `bagian`
--
ALTER TABLE `bagian`
  ADD PRIMARY KEY (`id_bagian`);

--
-- Indexes for table `ip`
--
ALTER TABLE `ip`
  ADD PRIMARY KEY (`id_ip`);

--
-- Indexes for table `izin`
--
ALTER TABLE `izin`
  ADD PRIMARY KEY (`id_izin`);

--
-- Indexes for table `presensi`
--
ALTER TABLE `presensi`
  ADD PRIMARY KEY (`id_presensi`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aktivitas`
--
ALTER TABLE `aktivitas`
  MODIFY `id_aktivitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `bagian`
--
ALTER TABLE `bagian`
  MODIFY `id_bagian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `ip`
--
ALTER TABLE `ip`
  MODIFY `id_ip` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `izin`
--
ALTER TABLE `izin`
  MODIFY `id_izin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `presensi`
--
ALTER TABLE `presensi`
  MODIFY `id_presensi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=180;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
