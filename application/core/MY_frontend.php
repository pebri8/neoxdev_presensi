<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_frontend extends MX_Controller {
	// protected $data;
	
	function __construct(){
		parent::__construct();
		$this->user = $this->session->userdata('my_id');
		$this->load->module('frontend/template');
		$this->load->model('my_model');
		
	}

	function premium(){
		if($this->user['tipe']=='pelajar') return $this->wd_db->get_row('pelajar',array('id'=> $this->user['tipe_id'],'status' => 1));
	}
	
}
?>