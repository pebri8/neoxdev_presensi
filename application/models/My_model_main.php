<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_model_main extends CI_Model {

	function karyawan($limit=null)
	{
		$this->db->join('bagian', 'bagian.id_bagian = user.id_bagian', 'left');
		// $this->db->join('presensi', 'presensi.id_user = user.id_user', 'inner');
		$this->db->order_by('user.id_user', 'desc');
		// $this->db->group_by('user.id_user');
		// $this->db->order_by('presensi.id_user', 'desc');
		if ($limit) {
			$this->db->limit($limit);
		}
		$query = $this->db->get('user');
		return $query->result_array();
	}

	function karyawan_row($device_id)
	{
		$this->db->join('bagian', 'bagian.id_bagian = user.id_bagian', 'left');
		$this->db->order_by('user.id_user', 'desc');
		$this->db->where('device_id', $device_id);
		// $this->db->order_by('presensi.id_user', 'desc');
		$query = $this->db->get('user');
		return $query->row_array();
	}

	function bagian()
	{
		$query = $this->db->get('bagian');
		return $query->result_array();
	}

	function izin()
	{
		$query = $this->db->get('izin');
		return $query->result_array();
	}

	function presensi($limit=null,$where=null)
	{
		$this->db->join('user', 'user.id_user = presensi.id_user', 'left');
		$this->db->join('bagian', 'bagian.id_bagian = user.id_bagian', 'left');
		$this->db->join('izin', 'izin.id_izin = user.id_izin', 'left');
		if ($limit) {
			$this->db->limit($limit);
		}
		if ($where) {
			$this->db->where($where);
		}
		$this->db->order_by('id_presensi', 'desc');
		$query = $this->db->get('presensi');
		return $query->result_array();
	}

	function presensi_where($where)
	{
		$this->db->join('user', 'user.id_user = presensi.id_user', 'left');
		$this->db->join('bagian', 'bagian.id_bagian = user.id_bagian', 'left');
		$this->db->join('izin', 'izin.id_izin = user.id_izin', 'left');
		$this->db->where($where);
		$this->db->order_by('id_presensi', 'desc');
		$query = $this->db->get('presensi');
		return $query->result_array();
	}

	function presensi_range($d_start,$d_end,$sum=null)
	{
		$this->db->join('user', 'user.id_user = presensi.id_user', 'left');
		$this->db->join('bagian', 'bagian.id_bagian = user.id_bagian', 'left');
		$this->db->join('izin', 'izin.id_izin = user.id_izin', 'left');

		$this->db->where("
			masuk >= CAST('".$d_start." 00:00:00' AS DATETIME) AND
			masuk <= CAST('".$d_end." 23:59:59' AS DATETIME)
		");

		$query = $this->db->get('presensi');
		return $query->result_array();
	}

	function presensi_sum_time($d_start,$d_end)
	{
		$this->db->join('user', 'user.id_user = presensi.id_user', 'left');
		$this->db->join('bagian', 'bagian.id_bagian = user.id_bagian', 'left');
		$this->db->join('izin', 'izin.id_izin = user.id_izin', 'left');

		$this->db->where("
			masuk >= CAST('".$d_start." 00:00:00' AS DATETIME) AND
			masuk <= CAST('".$d_end." 23:59:59' AS DATETIME)
		");

		$query = $this->db->get('presensi');
		$list = $query->result_array();
		$selisih = 0;
		$selisih_fix = 0;
		foreach ($list as $key) {
            $h_masuk = explode(' ', $key['masuk']);
            $h_masuk = explode(':', $h_masuk[1]);
            $hr_m_fix = ($h_masuk[0]*60)+$h_masuk[1];

            $h_pulang = explode(' ', $key['pulang']);
            $h_pulang = explode(':', $h_pulang[1]);
            $hr_p_fix = ($h_pulang[0]*60)+$h_pulang[1];

            $selisih = $selisih + $hr_p_fix - $hr_m_fix;

            $t_hr = $selisih;
            $selisih_fix = floor($t_hr/60)."h ".($t_hr%60)."m";
		}

		return $selisih;
	}

	public function check_ip($ip)
	{
		$this->db->where('ip_address', $ip);
		$r = $this->db->get('ip');
		return $r->result_array();
	}

	public function get_aktivitas($limit=null,$where=null)
	{
		if ($limit) {
			$this->db->limit($limit);
		}
		if ($where) {
			$this->db->where($where);
		}
		$this->db->join('user', 'aktivitas.id_user = user.id_user', 'left');
		$this->db->order_by('id_aktivitas', 'desc');
		$q = $this->db->get('aktivitas');
		return $q->result_array();
	}

	function device_id_check4()
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  // CURLOPT_URL => "http://api.ipify.org/?format=json",
		  CURLOPT_URL => '159.65.133.91/presensi/auth/resolusi/',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache"
		  ),
		));

		return $response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);
	}

	public function device_id_check()
	{
		// $url = 'http://159.65.133.91/presensi/auth/resolusi';
		$url = base_url().'auth/resolusi';
		// $url = urlencode($url);
		$content = file_get_contents($url);
		// $first_step = explode( 'id="ini_dev_id">' , $content );
		// $second_step = explode('</div>' , $first_step[1] );
		// $raised = trim($second_step[0]);
		// return $raised;
		// return $first_step[0];
		return $content;
	}

}

/* End of file My_model_main.php */
/* Location: ./application/models/My_model_main.php */

