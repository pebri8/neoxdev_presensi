<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	function siswa($id_user){
		$q = "select a.nama , b.nis, b.id, b.wali_id as wali_murid_id,
			b.kelas_id, c.wali_kelas as wali_kelas_id, c.sekolah as sekolah_id,
			d.nama as kelas, b.status , b.kode
			from user a 
			inner join pelajar b on a.tipe_id=b.id
			left join kelas c on b.kelas_id=c.id
			left join kelas_kat d on c.kategori=d.id
			where a.id='$id_user' ";
		return $this->db->query($q)->row_array();
	}

	function sekolah($id_user){
		$q = "select 
			a.nama, a.alamat, a.email,a.no_telp,
			b.propinsi as propinsi_id, c.propinsi, 
			b.kabupaten as kabupaten_id, d.kabupaten, 
			b.kecamatan as kecamatan_id, e.kecamatan,
			b.kode
			from user a 
			left join sekolah b on a.tipe_id=b.id
			left join propinsi c on b.propinsi=c.id
			left join kabupaten d on b.kabupaten=d.id
			left join kecamatan e on b.kecamatan=e.id
			where a.tipe_id='$id_user'";
		return $this->db->query($q)->row_array();
	}

	function playlist($set=array(),$select='*'){
		
		$this->db->select($select);
		if(isset($set['where'])) $this->db->where($set['where']);
		$this->db->from('playlist');
		if(isset($set['join'])) foreach ($set['join'] as $key => $value) $this->db->join($key, $value, 'left');
       	if (isset($set['limit'])) {
	       	if (isset($set['start'])) $this->db->limit($set['limit'],$set['start']);
	       	else $this->db->limit($set['limit']);
       	}
       	return $this->db->get();
       	
	}

	function playlist_detail($where){
		$this->db->select('playlist_detail.id, materi.id as materi_id, nama, durasi');
		$this->db->from('playlist_detail');
		$this->db->join('materi', 'playlist_detail.materi_id=materi.id', 'left');
		$this->db->where($where);
		return $this->db->get()->result_array();
	}

	function img_src($src,$path,$replace="assets/frontend/images/_physics.jpg"){
		if(substr($path, -1)!='/') $path.='/';

		if ($src=='') {
			$img=base_url().$replace;	
		}else{
			if (file_exists($path.$src)) {
				$img=base_url().$path.$src;
			}else{
				$img=base_url().$replace;
			}
		}
		return $img;	
	}

	function materi($tipe='audio',$premium=FALSE,$set ){
		// $set = array( id ,	limit,	start, count, semester, search ,id_kategori)
		// materi a , grade b , materi_kategori c 
		if(isset($set['search']) && $set['search'])	$this->db->like('a.nama', $set['search'], 'BOTH');

		$where = $tipe ? array('a.tipe_file' => $tipe) : array();
		if (isset($set['id'])) {
			$select = 'a.* , b.nama as grade_name, c.nama_kat';
			$where['a.id'] = $set['id']; 
		}else{
			$select ='a.tipe_file, a.semester,a.thumbnail, a.id, a.status, a.nama, a.author, b.nama as grade_name, a.durasi, a.viewers, a.file, c.nama_kat as kategori, c.id as kategori_id' ;
		}
		if(isset($set['semester'])) $where['a.semester'] = $set['semester'];
		if(isset($set['id_kategori'])) $where['c.id'] = $set['id_kategori'];
		
		$this->db->select($select);
		if(count($where)>0) $this->db->where($where);
		$this->db->from('materi a');
		$this->db->join('grade b', 'a.grade = b.id', 'left');
		$this->db->join('materi_kategori c', 'a.kategori=c.id', 'left');
       	if (isset($set['limit'])) {
	       	if (isset($set['start'])) $this->db->limit($set['limit'],$set['start']);
	       	else $this->db->limit($set['limit']);
       	}
       	$e = $this->db->get();
// debug($this->db->last_query());
       	if (isset($set['count'])) return $e->num_rows();

		if (!isset($set['id'])) {
			$e = $e->result_array();
	        if ($e) foreach ($e as $key => $val) {        	
				if (!filter_var($e[$key]['file'], FILTER_VALIDATE_URL)) $e[$key]['file'] = base_url('public/materi_file/'.$val['file']);
	        	if (!$premium && $val['status']==1) $e[$key]['file'] = '' ;
	        	$e[$key]['thumbnail'] = $this->img_src('crop_'.$e[$key]['thumbnail'],'public/materi_thumbnail/thumb');
			}
			return $e;
		}else{
			$e = $e->row_array();
			if($e){				
				$tipe_users = array('d' => 'Dengerin','p'=>'Pelajar' ,'g' => 'Guru');
				if (isset($tipe_users[$e['tipe_user']])) $e['tipe_user'] = $tipe_users[$e['tipe_user']];
				if (!filter_var($e['file'], FILTER_VALIDATE_URL)) $e['file'] = base_url('public/materi_file/'.$e['file']);
	    		if (!$premium && $e['status']==1) $e['file'] = '' ;
	    		$e['img_src'] = $e['thumbnail'];
	        	$e['thumbnail'] = $this->img_src('crop_'.$e['thumbnail'],'public/materi_thumbnail/thumb');

			}
			return $e;
		}

	}

	function ujian_list($id='',$custom=array()){
        $jnis_ujian = array(1=>'Ujian Harian', 'Tugas', 'Kuis','MID', 'UAS' );
        $where = array();
        $q = "select a.*,  b.nama_kat as kategori, c.soal_esay, d.soal_pilihan,

            case a.jenis_ujian ";
        foreach ($jnis_ujian as $key => $value) $q .= "when '$key' then '$value'";

        $q .= "else ''
            end as 'jenis_ujian_name'
	        from ujian a 
	        left join materi_kategori b on a.materi_kategori_id=b.id 
	        left join( select count(soal) as soal_esay, ujian_id from soal WHERE tipe=2 group by ujian_id) c on a.id=c.ujian_id
	        left join( select count(soal) as soal_pilihan, ujian_id from soal WHERE tipe=1 group by ujian_id) d on a.id=d.ujian_id ";

	    if ($id) $where[]= " a.materi_kategori_id = '$id'";	    
        if(count($custom)>0) foreach ($custom as $w) $where[] = $w;
	    
	    if(count($where)>0) {
	    	$where =implode(' and ', $where);
	    	$q.= " where $where ";			
		}
    	return $this->db->query($q)->result_array();
	}

	function get($set){
		$select = (isset($set['select'])) ? $set['select'] : '*';
		$this->db->select($select);
		$this->db->from($set['from']);
		if (isset($set['join'])) foreach ($set['join'] as $tble => $on) $this->db->join($tble, $on, 'left');
		if (isset($set['where'])) $this->db->where($set['where']);
       	if (isset($set['limit'])) {
	       	if (isset($set['start'])) $this->db->limit($set['limit'],$set['start']);
	       	else $this->db->limit($set['limit']);
       	}
       	return $this->db->get();
	}

	function kelas($ujian_id){
		return $this->db->query(
			'select distinct(b.kelas_id) as kelas
			from nilai a 
			left join pelajar b on a.siswa_id=b.id
			left join soal c on a.soal_id=c.id
			where c.ujian_id='.$ujian_id
			)->result_array();
	}

	function hasil_ujian($ujian_id, $siswa_id, $tipe,$koreksi=FALSE){
		$query = 'select a.*, b.soal, b.jawaban, b.pilihan, b.tipe, b.point, b.gambar, b.penjelasan
		 	from soal b
		 	left join nilai a on a.soal_id=b.id
		 	where a.siswa_id="'.$siswa_id.'"
		 	and b.tipe='.$tipe.'
		 	and b.ujian_id='.$ujian_id ;
		if ($koreksi) $query .= ' and a.koreksi="0"';
		 	$sql =  $this->db->query($query);
		return (!$koreksi) ? $sql->result_array() : $sql->num_rows() ;
	}

		
	function add_get_id($table,$data){
	   	$this->db->insert($table, $data);
	   	$insert_id = $this->db->insert_id();
	   	return $insert_id;
	}

	function get_data($table,$where=array(),$limit='',$start='',$select='*') {
		$this->db->select($select);
		$this->db->from($table);
		if (count($where)>0) $this->db->where($where);       	
       	if ($limit!='') {       		
	       	if ($start=='') $this->db->limit($limit);
	       	else $this->db->limit($limit,$start);
       	}
       	$q = $this->db->get();
        return $q -> result_array();
    }
    
	function new_id($table,$field,$inisial)	{	
		$strQuery = "select max($field) as $field from $table where $field like '$inisial%'";
		$data_lama = $this->db->query($strQuery);
		$data_lama = $data_lama->row();
		$data_lama = $data_lama->$field;		
		$panjang	= 8;
		
 		if ($data_lama=="") {
 			$data_baru = 0;
		}else {
 			$data_baru = substr($data_lama, strlen($inisial));
 		}
	
 		$data_baru++;
 		$data_baru	=strval($data_baru); 
 		$tmp	="";
 		for($i=1; $i<=($panjang-strlen($inisial)-strlen($data_baru)); $i++){
			$tmp=$tmp."0";	
		}
 		return $inisial.$tmp.$data_baru;
	}

    function get_row($table,$filed_array=array(),$select='*') {
		$this->db->select($select);
		$this->db->from($table);
        if(count($filed_array)>0) $this->db->where($filed_array);        
        $query = $this->db->get();
		return $query->row_array();
    } 

    function getCategory() {
    	$this->db->select('materi_kategori.*');
    	$this->db->from('materi_kategori');
		$this->db->join('ujian','materi_kategori.id = ujian.materi_kategori_id','left');
		$this->db->join('materi','materi_kategori.id = materi.kategori','left');
		$this->db->join('soal','soal.ujian_id = ujian.id','left');
		$this->db->having('COUNT(soal.id) > 0');
		$this->db->group_by('materi_kategori.id'); 
/*		$this->db->where('materi.status =',1);*/
		$query = $this->db->get();
		return $query->result_array(); 
    }
    function getUjian($id='') {
		$this->db->select('ujian.*, ujian.id as id_ujian, ujian.nama as nama_ujian', FALSE);
		$this->db->select('materi_kategori.*, materi_kategori.id as id_materi_kategori', FALSE);
		$this->db->select('materi.id, materi.nama, materi.status, materi.id as id_materi, materi.nama as nama_materi', FALSE);
		$this->db->select('soal.*, soal.id as id_soal', FALSE);
		$this->db->from('ujian');
		$this->db->join('materi_kategori','materi_kategori.id = ujian.materi_kategori_id','left');
		$this->db->join('materi','materi_kategori.id = materi.kategori','left');
		$this->db->join('soal','soal.ujian_id = ujian.id','left');
		$this->db->distinct();
    	$this->db->group_by('ujian.id');
    	$this->db->group_by('soal.ujian_id');
// 		$this->db->where('materi.status =',1);
    	$this->db->where('ujian.materi_kategori_id =',$id);
    	$this->db->having('COUNT(soal.id) > 0');
		$query = $this->db->get();
		return $query->result_array(); 
		// select ujian.*, ujian.id as id, materi_kategori.*, materi_kategori.id as id_materi_kategori, materi.id, materi.nama, materi.status,
		// materi.id as id_materi, materi.nama as nama_materi, soal.*, soal.id as id_soal
		// from ujian
		// left join materi_kategori on materi_kategori.id = ujian.materi_kategori_id
		// left join materi on materi_kategori.id = materi.kategori
		// left join soal on soal.ujian_id =ujian.id
		// distinct
		// group_by ujian.id
		// where materi.status = 1
		// where ujian.materi_kategori_id = 9
    }

    function ujian_details($id_ujian) {
    	$this->db->select('ujian.*, ujian.id as id_ujian, ujian.nama as nama_ujian', FALSE);
		$this->db->select('materi_kategori.*, materi_kategori.id as id_materi_kategori', FALSE);
		$this->db->from('ujian');
		$this->db->join('materi_kategori','materi_kategori.id = ujian.materi_kategori_id','left');
		$this->db->where('ujian.id',$id_ujian);
		$query = $this->db->get();
		return $query->row();
		//debug($hasil);
    }

    function track_ujian($id_ujian, $id_siswa) {
		$this->db->select('selesai');
		$this->db->from('track_ujian');
		$this->db->where('ujian_id',$id_ujian);
		$this->db->where('siswa_id',$id_siswa);
		$query = $this->db->get();
		return $query->row();
    }
	
    function jml_soal($id) {
    	$this->db->select('soal.*, soal.id as id_soal', FALSE);
		$this->db->select('ujian.*, ujian.id as id_ujian, ujian.nama as nama_ujian', FALSE);
		$this->db->from('soal');
		$this->db->join('ujian','soal.ujian_id = ujian.id','left');
    	$this->db->where('ujian.id', $id);
    	$query = $this->db->get();
		return $query->result_array();
    }
    function get_soal($id) {
    	$this->db->select('soal.*, soal.id as id_soal', FALSE);
    	$this->db->from('soal');
  		$this->db->where('id', $id);
    	$query = $this->db->get();
		return $query->row();
    }
    function countsoal($id) {
    	$this->db->select('soal.id as id_soal', FALSE);
		$this->db->select('ujian.mulai, ujian.selesai', FALSE);
		$this->db->from('soal');
		$this->db->join('ujian','soal.ujian_id = ujian.id','left');
    	$this->db->where('ujian.id', $id);
    	$query = $this->db->get();
		return $query->result_array();
    }
	function update ($table,$data, $where) {
		$fields = $this->db->list_fields($table);
		$newData = array();
		foreach ($data as $key => $value) {
			if (in_array($key,$fields))
				$newData[$key] = $value;
		} 
		
		$this->db->where($where);
		return $this->db->update($table,$newData);
	}

	
}

/* End of file My_model.php */
/* Location: .//mnt/70bda58b-6fd9-d101-20a1-a58b6fd9d101/public_html/dengerin/api/api/models/My_model.php */