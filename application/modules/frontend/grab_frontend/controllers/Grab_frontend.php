<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grab_frontend extends CI_Controller {

	public $data;
	public $rules;

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		// if (empty($this->session->userdata('admin_login'))) {
			// redirect('auth');
		// }
		date_default_timezone_set ( "Asia/Jakarta" );
	}

	public function view($data = NULL,$tipe=true)
	{
		$this->load->view('grab_frontend/index', $data);
	}

	public function upload_single($error_link)
	{
		// debug($this->input->post());
		$id = $this->input->post('id');
		$img_lama = $this->input->post('img_lama');

		// cek user tidak merubah img saat edit data
		if ($id) {
			if ($_FILES['file']['name'][0] == '') {
				return $img_lama;
			}
		}

		$files = $_FILES;
		$cpt = count($_FILES['file']['name']);
		for ($i=0; $i < $cpt; $i++) {
	        $_FILES['file']['name']= $files['file']['name'][$i];
	        $_FILES['file']['type']= $files['file']['type'][$i];
	        $_FILES['file']['tmp_name']= $files['file']['tmp_name'][$i];
	        $_FILES['file']['error']= $files['file']['error'][$i];
	        $_FILES['file']['size']= $files['file']['size'][$i];

	        $config['upload_path']          = './'.$this->folder_file;
	        $config['allowed_types']        = $this->file_allowed_types;
    		// $config['max_size']             = 35;
	        $this->upload->initialize($config);
	        if ( ! $this->upload->do_upload('file')){
		        $file_data = $this->upload->data();
	        	// echo $this->upload->display_errors();
        		// @unlink('./'.$this->folder_file.'/'.$file_data['file_name']);
        		// @unlink('./'.$this->folder_thumb_file.'/thumb_'.$file_data['file_name']);
				$this->session->set_flashdata('alert_warning', $_FILES['file']['name'].' '.$this->upload->display_errors());
				redirect($error_link);
	        }else{
				// resize
		        $file_data = $this->upload->data();
				$config['image_library'] = 'gd2';
				$config['source_image'] = './'.$this->folder_file.'/'.$file_data['file_name'];
				$config['new_image'] = './'.$this->folder_thumb_file.'/thumb_'.$file_data['file_name'];
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = TRUE;
				$config['width']         = $this->resize_width;
				$config['height']       = $this->resize_height;

				$this->load->library('image_lib', $config);

				$this->image_lib->resize();
				$this->image_lib->initialize($config);

				if ( ! $this->image_lib->resize()){
					// echo $this->image_lib->display_errors();
	        		@unlink('./'.$this->folder_file.'/'.$file_data['file_name']);
	        		@unlink('./'.$this->folder_thumb_file.'/thumb_'.$file_data['file_name']);
					$this->session->set_flashdata('alert_warning', $_FILES['file']['name'].' '.$this->image_lib->display_errors());
					redirect($error_link);
				}else{
					if ($id) {
		        		@unlink('./'.$this->folder_file.'/'.$img_lama);
		        		@unlink('./'.$this->folder_thumb_file.'/thumb_'.$img_lama);
					}
					return $file_data['file_name'];
				}
	        }
		}
	}

	public function check_ip_now()
	{
		$this->akses();
		if (!$this->session->userdata('success_check_ip')) {
			redirect('');
		}else if (!$this->session->userdata('user_logged')) {
			redirect('');
		}else{
			return true;
		}
	}

	public function akses()
	{
		$ip = $_SERVER["REMOTE_ADDR"];
		
		$this->session->unset_userdata('user_logged');
		$this->session->unset_userdata('success_check_ip');

		// $device_id=strtolower($_SERVER['HTTP_USER_AGENT']);
		// $pecah = explode("(", $device_id);
		// $pecah = explode(")", $pecah[1]);
		// $device_id = $pecah[0];
		// debug($device_id);

		// cek ip di database ada yang sama atau tidak
		if(
			$ip!="127.0.0.1" && 
			$ip!="192.168.1.23" && 
			$ip!="192.168.1.139" && 
			// $ip!="192.168.1.22" && 
			$ip!="114.142.171.55" && 
			substr($ip, 0, 10) != "36.72.218." && 
			// $_SERVER["REMOTE_ADDR"] != "180.253.134.164" && 
			// $_SERVER["REMOTE_ADDR"] != "36.81.25.44" && 
			// $_SERVER["REMOTE_ADDR"] != "36.73.56.181" && 
			// substr($_SERVER["REMOTE_ADDR"], 0, 9) != "202.91.9." && 
			// substr($_SERVER["REMOTE_ADDR"], 0, 11) != "202.65.121." && 
			// substr($_SERVER["REMOTE_ADDR"], 0, 11) != "103.86.100." && 
			$ip!= "::1"){

				// jika tidak sama maka cek di database
				$ip_database = $this->My_model_main->check_ip($ip);
				if (count($ip_database) >= 1) {
					// bila ditemukan ip yang cocok
					$sess_data['success_check_ip'] = 'ok_in_database';
				}else{
					$sess_data['error_check_ip'] = 'error';
				}

		}else{
			$sess_data['success_check_ip'] = 'ok_in_function';
		}

		//jika device id tidak ada
		if ($this->session->userdata('device_id2') == '') {
			redirect('auth/getting_device_id');
		}

		// cek user dengan device_id
		$device_id = $this->session->userdata('device_id2');

		$user_info = $this->My_model_main->karyawan_row($device_id);
		// debug($user_info);
		if ($jml_user = count($user_info) >= 1) {
			if ($jml_user > 1) {
				debug('Maaf, terjadi kesalahan. Hubungi pihak development! kode:0001');
			}
			if ($user_info['status_login'] == '1') {
				$sess_data['id_user_presensi'] = $user_info['id_user'];
				$sess_data['user_logged'] = 'ok';
			}
			$sess_data['device_status'] = 'ok';
		}

		$sess_data['ip_user'] = $ip;
		$sess_data['device_id'] = sha1($device_id);
		$this->session->set_userdata($sess_data);
		// debug($this->session->userdata());
	}

}

/* End of file Grab.php */
/* Location: ./application/modules/backend/grab/controllers/Grab.php */