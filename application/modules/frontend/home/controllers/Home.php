<?php
defined('BASEPATH') OR exit('No direct script access allowed'); require 'application/modules/frontend/grab_frontend/controllers/Grab_frontend.php';

class Home extends Grab_frontend {

	public function __construct()
	{
		parent::__construct();
		if (!$this->session->userdata('success_check_ip') OR !$this->session->userdata('user_logged')) {
			redirect('auth');
		}
		$this->user_info = $this->My_model_main->karyawan_row($this->session->userdata('device_id2'));
		$this->load->model('My_model');
		$this->date_today = date("Y-m-d H:i:s");
	}

	public function index()
	{
		$this->check_ip_now();

		$data['list_users'] = $this->user_info;
		$data['list_presensi_users'] = $this->My_model->getPresensi(array('id_user' => $this->user_info['id_user'], ),'5');
		$csp = $this->My_model->cek_status_presensi();
		// debug($csp);
		$data['status_presensi_users'] = isset($csp[0]['status_presensi']) ? $csp[0]['status_presensi'] : '';

		if (count($csp) <= 0) {
			$presensi_ke = 1;
		}else{
			if ($csp[0]['status_presensi'] == 1) {
			 	$presensi_ke = count($csp)+1;
			 }else{
			 	$presensi_ke = count($csp);
			 }
		}
		$data['presnsi_ke'] = $presensi_ke ; // presensi ke ... hari ini
        $data['content'] = "form";
		$this->view($data,false);
	}

	public function presensi_masuk()
	{
		// debug($this->input->post());
		$this->check_ip_now();

		// cek hari ini udah presensi masuk apa belum { status presensi hari ini}
		// $q = $this->My_model->check_presensi_today();
		// if (count($q) > 0) {
		// 	$this->session->set_flashdata('alert_error', 'Anda sudah presensi masuk, silahakan presensi lagi besok :)');
		// 	redirect('home');
		// }

		$q = $this->My_model->cek_status_presensi();
		$status_presensi = isset($q[0]['status_presensi']) ? $q[0]['status_presensi'] : 'bukan';
		if ($status_presensi == '0') {
			$this->session->set_flashdata('alert_error', 'Silahkan presensi pulang dahulu untuk bisa presensi masuk :)');
			redirect('home');
		}

		// input presensi
		$ob = array(
			'id_user' => $this->session->userdata('id_user_presensi'),
			'masuk' => $this->date_today,
			'pulang' => $this->date_today,
			'status_presensi' => '0',
			'keterangan_masuk' => $this->input->post('keterangan'),
		);
		$qi = $this->db->insert('presensi', $ob);
		if ($qi) {
			$this->session->set_flashdata('alert_success', 'Selamat kamu berhasil presensi masuk');
			redirect('home');
		}
	}

	public function presensi_pulang()
	{
		$this->check_ip_now();

		// cek hari ini udah presensi pulang apa belum
		$q = $this->My_model->check_presensi_today();
		if (count($q) > 0) {

			// cek apakah sudah presensi pulang
			if ($q['status_presensi'] == '0') {
				
				// input presensi
				$ob = array(
					'pulang' => $this->date_today,
					'status_presensi' => '1',
					'keterangan_pulang' => $this->input->post('keterangan'),

				);
				// $this->db->limit(1);
				$this->db->where('id_presensi', $q['id_presensi']);
				$qu = $this->db->update('presensi', $ob);
				if ($qu) {
					$this->session->set_flashdata('alert_success', 'Selamat kamu berhasil presensi istirahat / pulang :)');
				}
			}else{
				$this->session->set_flashdata('alert_error', 'Kamu sudah presensi pulang, Enjoy your life!');
			}
		}else{
			$this->session->set_flashdata('alert_error', 'Kamu belum presensi masuk!');
		}
		redirect('home');

	}

	public function error()
	{
        $data['content'] = "error";
		$this->view($data,false);
	}

	public function bahagia()
	{
        $data['content'] = "form";
		$this->view($data,false);
	}

	public function cek()
	{
		echo "string";
	}


}

/* End of file Home.php */
/* Location: ./application/modules/frontend/home/controllers/Home.php */