<?php include 'css-popup.php'; ?>
<div class="content_p">
	<div class="title_presensi">
		<img src="<?php echo base_url() ?>assets/img/logo_neox.svg" width="150px" alt="">
	</div>
	<div class="title_presensi">
		PRESENSI BAHAGIA
	</div>
	<div class="history_p">
		<span>Riwayat</span>
		<div class="l_hp">
			<!-- asd -->
			<table class="tbl_p">
				<thead>
					<tr>
						<th>Masuk</th>
						<th>Pulang</th>
					</tr>
				</thead>
				<tbody>
					<style>
						.lbl_pre_today:first-child{
							border: 1px solid;
							border-radius: 10px;
							background-color: #8ec63f;
							color: white;
						}
						.sts_waktu_today:first-child{
							font-weight: bold;
						}
					</style>
					<?php $no=1; ?>
					<?php foreach ($list_presensi_users as $key): ?>
					<tr>
						<td><div class="<?php if($status_presensi_users!="" AND $no==1){echo "sts_waktu_today";} ?>"><?php echo $key['masuk']; ?></div></td>
						<td><div class="<?php if($status_presensi_users!="" AND $no==1){echo "sts_waktu_today";} ?>"><?php echo $key['pulang'] != $key['masuk'] ? $key['pulang'] : substr($key['pulang'], 0,10).' --:--:--'; ?></div></td>
					</tr>
					<?php $no++; ?>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>

	<p style="margin-bottom: 0px">Hello, <?php echo $list_users['nama']; ?> :)</p>
  <p>Presensi ke-<?php echo $presnsi_ke; ?></p>

  <a class="btn_masuk_2" href="">
    <button <?php if($status_presensi_users=="0"){echo " disabled"; $pm="(&#10004;)";} ?> class="btn btn-primary btn_presensi col-xs-12" type="">Presensi Masuk <?php echo isset($pm) ? $pm : ''; ?></button>
  </a>
  <a class="btn_pulang_2" href="<?php echo base_url() ?>home/presensi_pulang">
    <button class="btn btn-warning btn_presensi col-xs-12" type="">Presensi Istirahat / Pulang</button>
  </a>

	<div class="clearfix"></div>
	<div class="mt">
	  <p>©2018 Hak Cipta. Neoxdev Indonesia.</p>
	</div>
</div>

<!-- popup presensi masuk -->
<div id="myNav" style="display: none;" class="overlay close-popup">
  <!-- <a href="javascript:void(0)" class="closebtn" id="close_video">&times;</a> -->
  <div class="overlay-content" style="height: 80%; top: 150px;">
    <div style="width: 250px; margin: auto;">
      <form action="<?php echo base_url() ?>home/presensi_masuk" method="POST">
        <div class="font-normal" style="margin-bottom: 5px;">
          Apa yang akan kamu kerjakan?
        </div>
        <textarea name="keterangan" class="form-control" rows="5" required="" style="margin-bottom: 10px;"></textarea>
      <button style="width: 45%;" id="close_video" class="btn btn-warning" type="button">Batal</button>
      <button style="width: 45%; float: right;" type="submit" class="btn btn-primary">Presensi</button>
      </form>
    </div>
  </div>
</div>

<!-- popup presensi keluar -->
<div id="myNav2" style="display: none;" class="overlay close-popup">
  <!-- <a href="javascript:void(0)" class="closebtn" id="close_video">&times;</a> -->
  <div class="overlay-content" style="height: 80%; top: 150px;">
    <div style="width: 250px; margin: auto;">
      <form action="<?php echo base_url() ?>home/presensi_pulang" method="POST">
        <div class="font-normal" style="margin-bottom: 5px;">
          Apa yang sudah kamu kerjakan?
        </div>
        <textarea name="keterangan" class="form-control" rows="5" required="" style="margin-bottom: 10px;"></textarea>
      <button style="width: 45%;" id="close_popup2" class="btn btn-warning" type="button">Batal</button>
      <button style="width: 45%; float: right;" type="submit" class="btn btn-primary">Horee</button>
      </form>
    </div>
  </div>
</div>

<script>
  $("a.btn_masuk_2").click(function(e){
    e.preventDefault();
    // $("a.btn_masuk_2").attr('href');
    document.getElementById('myNav').style.display="block";
  });
  $("button#close_video").click(function(e){
    e.preventDefault();
    document.getElementById('myNav').style.display="none";
  });
</script>

<script>
  $("a.btn_pulang_2").click(function(e){
    e.preventDefault();
    // $("a.btn_masuk_2").attr('href');
    document.getElementById('myNav2').style.display="block";
  });
  $("button#close_popup2").click(function(e){
    e.preventDefault();
    document.getElementById('myNav2').style.display="none";
  });
</script>



<!-- <script>
  $('a.btn_masuk').confirm({
      title: 'Confirm?',
      content: 'Anda yakin akan melakukan presensi hari ini?',
      buttons: {
          cancel: function () {
          },
          confirm: function () {
              location.href = this.$target.attr('href');
          },
      }
  });
</script> -->

<!-- <script>
  $('a.btn_pulang').confirm({
      title: 'Confirm?',
      content: 'Anda yakin akan melakukan presensi pulang hari ini?',
      buttons: {
          batal: function () {
          },
          horee: function () {
              location.href = this.$target.attr('href');
          },
      }
  });
</script> -->

<?php if ($this->session->flashdata('alert_error')): ?>
<script>
	$.alert({
	    title: 'Error!',
	    content: '<?php echo $this->session->flashdata('alert_error') ?>',
	});
</script>
<?php endif ?>

<?php if ($this->session->flashdata('alert_success')): ?>
<script>
	$.alert({
	    title: 'Success!',
	    content: '<?php echo $this->session->flashdata('alert_success') ?>',
	});
</script>
<?php endif ?>