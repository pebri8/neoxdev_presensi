<style type="text/css" media="screen">
  .overlay {
      height: 100%;
      width: 100%;
      /*display: none;*/
      position: fixed;
      z-index: 1031;
      top: 0;
      left: 0;
      background-color: rgb(0,0,0);
      background-color: rgba(0,0,0, 0.9);
  }

  .overlay-content {
      position: relative;
      top: 5%;
      width: 100%;
      text-align: center;
      margin-top: 30px;
  }

  .overlay a {
      padding: 8px;
      text-decoration: none;
      font-size: 36px;
      color: #818181;
      display: block;
      transition: 0.3s;
  }

  .overlay a:hover, .overlay a:focus {
      color: #f1f1f1;
  }

  .overlay .closebtn {
      position: absolute;
      top: 0px;
      right: 45px;
      font-size: 60px;
  }

  @media screen and (max-height: 450px) {
    .overlay a {font-size: 20px}
    .overlay .closebtn {
      font-size: 40px;
      top: 15px;
      right: 35px;
    }
  }

  .on_top_video {
    align-items: center;
      position:absolute;
      top:0;
      left:0;
      width:100%;
      height:220px;
      background-color: green;
      opacity:0;
      /*background:rgba(255,255,255,0.8); or just this*/
      z-index:50;
      color:#fff;
  }

  .link_on_top_video{
    position: absolute;
    width: 92%;
    height: 220px;
    background-color: blue;
    z-index: 51;
  }

  .font-normal{
  	text-shadow: none;
  	color: white;
  	/*font-style: normal;*/
  	/*font-family: Comic San Ms;*/
  	font-size: 15px;
  }
</style>