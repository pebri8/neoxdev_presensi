            <form action="auth/cek_user" method="post">
              <h1>Login Form</h1>
              <div>
                <input type="email" name="email" class="form-control" placeholder="Email" required="" />
              </div>
              <div>
                <input type="password" name="password" class="form-control" placeholder="Kata Sandi" required="" />
              </div>
              <div>
                <button class="btn btn-default submit" type="submit">Masuk</button>
                <!-- <a class="reset_pass" href="auth/forgot">Lupa kata sandi?</a> -->
              </div>

              <div class="clearfix"></div>

              <div class="separator">

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> Neox Indonesia!</h1>
                  <img src="<?php echo base_url() ?>assets/admin/images/login/faviconn.svg" width="180px">
                  <br>
                  <p>©2018 Hak Cipta. Neox Indonesia!</p>
                </div>
              </div>
            </form>