<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_model extends CI_Model {

	function getPresensi($where,$limit)
	{
		$this->db->where($where);
		$this->db->limit($limit);
		$this->db->order_by('id_presensi', 'desc');
		$q = $this->db->get('presensi');
		return $q->result_array();
	}

	function check_presensi_today()
	{
		$this->db->where('id_user', $this->session->userdata('id_user_presensi'));
		$this->db->like('masuk', date("Y-m-d"), 'BOTH');
		// $this->db->like('masuk', '2018-03-07', 'BOTH');
		$this->db->order_by('id_presensi', 'desc');
		$q = $this->db->get('presensi');
		return $q->row_array();
	}

	function cek_status_presensi()
	{
		// cek apakah kemarin sudah presensi pulang
		$this->db->where('id_user', $this->session->userdata('id_user_presensi'));
		$date_yesterday = date('Y-m-d',strtotime(date('Y-m-d') . "-1 days"));
		$this->db->where("(
			masuk <= CAST('".$date_yesterday." 23:59:59' AS DATETIME)
			)", NULL, FALSE);
		$this->db->limit(1);
		$this->db->order_by('id_presensi', 'desc');
		$q2 = $this->db->get('presensi')->result_array();
		// debug($q2);
		if ($q2[0]['status_presensi'] == 0) {
			// ambil tanggal terakhir presensi jika tidak presensi pulang
			$plus_last = substr($q2[0]['masuk'], 0, 10);
		}


		$this->db->where('id_user', $this->session->userdata('id_user_presensi'));
		if (isset($plus_last) == true) {
			$this->db->where("(
				masuk LIKE '%".$plus_last."%' OR
				masuk LIKE '%".date('Y-m-d')."%'
				)", NULL, FALSE);
		}else{
			$this->db->where("(
				masuk LIKE '%".date('Y-m-d')."%'
				)", NULL, FALSE);
		}
		$this->db->order_by('id_presensi', 'desc');
		$q = $this->db->get('presensi')->result_array();
		return $q;
	}
}

/* End of file My_model.php */
/* Location: ./application/modules/frontend/home/models/My_model.php */