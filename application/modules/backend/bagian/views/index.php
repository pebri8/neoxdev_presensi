            <div class="page-title">
              <div class="title_left">
                <h3>Bagian</h3>
              </div>

            </div>

            <div class="clearfix"></div>

            <?php require_once __DIR__."/../../blocks/alert_notification.php"; ?>

            <div class="row">
              <div class="clearfix"></div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Daftar Bagian <small>Halaman utama</small></h2>

                    <form action="" method="get" accept-charset="utf-8">
                    <div class="filter" style="width: 50%; min-width: 280px;">
                      <button type="submit" class="pull-right" type="">Cari</button>
<!--                       <div class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; margin-right: 10px;">
                        <input id="ddd" name="range_date" value="<?php echo $this->input->get('range_date') ?>" type="text" data-multiple-dates-separator=" - " placeholder="Date Range" style="background: #fff; padding: 0px 3px; border: none;">
                      </div> -->
                      <div class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; margin-right: 10px;">
                        <input type="text" name="keyword" value="<?php echo $this->input->get('keyword') ?>" placeholder="Keyword" style="background: #fff; padding: 0px 3px; border: none;">
                      </div>
                    </div>
                    </form>

                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">

                    <form action="<?php echo base_url() ?>backend/<?php echo $this->modul ?>/delete_bulk" method="post" accept-charset="utf-8">
                    <a href="<?php echo $this->modul ?>/form" class="btn btn-primary" title="">Tambah Bagian</a>
                    <button class="btn btn-danger delete_bulk">Bulk Delete</button>
                    <button hidden type="submit" id="delete_bulk_ok"></button>
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th>
                              <input type="checkbox" id="check_all" class="flat">
                            </th>
                            <th class="column-title">Nama Bagian </th>
                            <th class="column-title no-link last"><span class="nobr">Action</span>
                            </th>
                            <th class="bulk-actions" colspan="7">
                              <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                          </tr>
                        </thead>

                        <tbody>
                        <?php foreach ($list_data as $dt): ?>
                          <tr class="odd pointer">
                            <td class="a-center ">
                              <input type="checkbox" class="flat" name="table_records[]" value="<?php echo $dt->id_bagian ?>">
                            </td>
                            <td class=" "><?php echo $dt->nama_bagian; ?></td>
                            <td class=" last">
                              <a href="<?php echo $this->modul ?>/form/<?php echo $dt->id_bagian ?>">Edit</a> |
                              <a href="<?php echo $this->modul ?>/delete/<?php echo $dt->id_bagian ?>" class="delete_self">Hapus</a>
                            </td>
                          </tr>
                        <?php endforeach ?>
                        </tbody>
                      </table>
                        <ul class="pagination no-margin pull-left">
                          <li>Total Rows: <?php echo $total_rows ?></li>
                        </ul>
                        <ul class="pagination pagination-sm no-margin pull-right">
                          <?php echo $this->pagination->create_links(); ?>
                        </ul>
                    </div>
                    </form>
							
						
                  </div>
                </div>
              </div>
            </div>

<script>
$('#ddd').datepicker({
    range: 'true',
    language: 'en',
    maxDate: new Date()
})
</script>

<script>
  $('#check_all').click(function(event) {
    if(this.checked) {
        // Iterate each checkbox
        $(':checkbox').each(function() {
            this.checked = true;
        });
    }
    else {
      $(':checkbox').each(function() {
            this.checked = false;
        });
    }
  });
</script>

<script>
  $('a.delete_self').confirm({
      title: 'Confirm?',
      content: 'Anda yakin akan menghapus data yang dipilih?',
      buttons: {
          confirm: function () {
              location.href = this.$target.attr('href');
          },
          cancel: function () {
          },
      }
  });
</script>

<script>
  $('button.delete_bulk').confirm({
      title: 'Confirm!',
      content: 'Anda yakin akan menghapus semua data yang dipilih?',
      buttons: {
          confirm: function () {
              $('#delete_bulk_ok').click();
          },
          cancel: function () {
          },
      }
  });
</script>