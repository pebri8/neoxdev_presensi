            <div class="page-title">
              <div class="title_left">
                <h3>Bagian / Form</h3>
              </div>

            </div>

            <div class="clearfix"></div>

            <?php require_once __DIR__."/../../blocks/alert_notification.php"; ?>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Form Tambah Bagian <small>Silahkan diisi</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form action="<?php echo base_url() ?>backend/<?php echo $this->modul ?>/save" method="post" id="form" data-parsley-validate class="form-horizontal form-label-left">
                    <input type="hidden" name="id" value="<?php echo $list ? $list['id_bagian'] : '' ?>">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Bagian <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="nama_bagian" value="<?php echo $list ? $list['nama_bagian'] : '' ?>" class="form-control col-md-7 col-xs-12" required>
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="submit" class="btn btn-success">Simpan</button>
						  <button class="btn btn-primary" type="reset">Reset</button>
                          <a class="btn btn-primary" href="<?php echo base_url() ?>backend/<?php echo $this->modul ?>">Kembali</a>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>

	<script>
	$('#ddd').datepicker({
	    range: 'true',
	    language: 'en',
	    maxDate: new Date()
	});
	</script>

	<script>
	$(document).ready(function() {
	    $('#select').select2();
	});
	</script>