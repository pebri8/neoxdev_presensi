<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_model extends CI_Model {

	var $table = 'bagian';

	public function hitung($object)
	{
    	$this->pagination_join_trip($object);
    	$this->pagination_where_trip($object);
    	$this->pagination_like_trip($object);
        $query = $this->db->get($this->table);
		return $query->num_rows();
	}

	public function getData($object)
	{		
		$this->pagination_like_trip($object);
		$this->pagination_join_trip($object);
		$this->pagination_where_trip($object);
		$this->db->order_by('id_bagian', 'desc');
		return $this->db->get($this->table, $object['limit'], $object['offset'])->result();
	}

	private function pagination_join_trip($object)
	{
		// $this->db->join('user', 'user.id_user = presensi.id_user', 'left');
		// $this->db->join('bagian', 'bagian.id_bagian = user.id_bagian', 'left');
	}

	private function pagination_where_trip($object)
	{
		// if ($object['d_r']) {
		// 	$d_start = date("Y-m-d", strtotime($object['d_r'][0]));
		// 	$d_end = date("Y-m-d", strtotime($object['d_r'][1]));
		// 	$this->db->where("
		// 		masuk >= CAST('".$d_start." 00:00:00' AS DATETIME) AND
		// 		masuk <= CAST('".$d_end." 23:59:00' AS DATETIME)
		// 	");
		// }
	}

	private function pagination_like_trip($object)
	{
		$this->db->where("(
			nama_bagian LIKE '%".$object['keyword']."%' OR
			nama_bagian LIKE '%".$object['keyword']."%'
			)", NULL, FALSE);
	}	

}

/* End of file My_model.php */
/* Location: ./application/modules/backend/presensi/models/My_model.php */