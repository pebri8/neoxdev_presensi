<?php
defined('BASEPATH') OR exit('No direct script access allowed'); require 'application/modules/backend/grab/controllers/Grab.php';

class Bagian extends Grab {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('My_model');
		$this->modul = "bagian";
		$this->tabel = 'bagian';
		$this->id_name = 'id_bagian';
	}

	public function index()
	{
        $this->load->library('pagination');
        $params['v'] = 'search';

        if ($this->input->get('keyword') == true) {
            $params['keyword'] = $this->input->get('keyword');
        }
        if ($this->input->get('range_date') == true) {
            $params['range_date'] = $this->input->get('range_date');
            $range_date = explode(' - ', $params['range_date']);
            // debug($aa);
        }
        if ($this->input->get('per_page') == true) {
            $params['per_page'] = $this->input->get('per_page');
        }

        $queryString =  http_build_query($params);
        $hpp = explode('&per_page', $queryString);

        $config['base_url'] = base_url().'backend/'.$this->modul.'?'.$hpp[0];
        $config['per_page'] = 5;
        $config['uri_segment'] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        // $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        // $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';        
        $config['last_link'] = 'Last';        
        $config['first_tag_open'] = '<li>';        
        $config['first_tag_close'] = '</li>';        
        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li>';      
        $config['prev_tag_close'] = '</li>';          
        $config['next_link'] = 'Next';        
        $config['next_tag_open'] = '<li>';        
        $config['next_tag_close'] = '</li>';        
        $config['last_tag_open'] = '<li>';        
        $config['last_tag_close'] = '</li>';        
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void()">';        
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';        
        $config['num_tag_close'] = '</li>';

        $offset = 0;
        if (!empty($_GET['per_page'])) {
            $pageNo = $_GET['per_page'];
            $offset = ($pageNo - 1) * $config["per_page"];
        }

        $object = array(
            'limit' => $config['per_page'],
            'offset' => $offset,
            'keyword' => $this->input->get('keyword'),
            'd_r' => isset($range_date) ? $range_date : '',
        );

        $config['total_rows'] = $this->My_model->hitung($object);
        $data['no_urut'] = $offset+1;
        $this->pagination->initialize($config);

        $data['total_rows'] = $this->My_model->hitung($object);
        $data['list_data'] = $this->My_model->getData($object);
        // debug($data['list_data']);

        $data['content'] = "index";
		$this->view($data,false);
	}

	public function form($id=null)
	{
		$data['list_users'] = $this->My_model_main->karyawan();
		$data['list'] = $this->wd_db->get_data_row($this->tabel, array($this->id_name => $id, ));
		// debug($data['list_users']);
		$data['content'] = "form";
		$this->view($data);
	}

	public function save()
	{
		$id = $this->input->post('id');
		$nm_bagian = $this->input->post('nama_bagian');
		$ob = array(
			'nama_bagian' => $nm_bagian,
		);
		if ($id) {
			$this->db->where($this->id_name, $id);
			$qi = $this->db->update($this->tabel, $ob);
		}else{
			$qi = $this->db->insert($this->tabel, $ob);
		}
		if ($qi) {
			if ($id) {
				$this->session->set_flashdata('alert_success', 'Data berhasil diedit');
				$this->create_activity($this->data_user_aktif['id_user'],'Mengedit data '.$nm_bagian.'pada menu bagian');
				redirect('backend/'.$this->modul.'/form/'.$id.'');
			}else{
				$this->session->set_flashdata('alert_success', 'Data berhasil ditambahkan');
				$this->create_activity($this->data_user_aktif['id_user'],'Menambah data '.$nm_bagian.'pada menu bagian');
				redirect('backend/'.$this->modul.'/form');
			}
		}else{
			if ($id) {
				$this->session->set_flashdata('alert_warning', 'Edit data');
				redirect('backend/'.$this->modul.'/form/'.$id.'');
			}else{
				$this->session->set_flashdata('alert_warning', 'Menambah data');
				redirect('backend/'.$this->modul.'/form');
			}
		}
	}

	public function delete($id)
	{
		// cek dipakai atau tidak
		$get_user = $this->wd_db->get_data('user',array('id_bagian' => $id,));
		$get_bagian = $this->wd_db->get_data('bagian',array('id_bagian' => $id,));
		if (count($get_user) > 0) {
			$this->session->set_flashdata('alert_warning', 'Tidak dapat menghapus data '.$get_bagian[0]['nama_bagian'].', Data masih terpakai oleh '.$get_user[0]['nama']);
			redirect('backend/'.$this->modul);
		}
		
		$this->db->where($this->id_name, $id);
		$qd = $this->db->delete($this->tabel);
		if ($qd) {
			$this->session->set_flashdata('alert_success', 'Data berhasil dihapus');
			$this->create_activity($this->data_user_aktif['id_user'],'Menghapus data '.$get_bagian[0]['nama_bagian'].' pada menu bagian');
			redirect('backend/'.$this->modul);
		}else{
			$this->session->set_flashdata('alert_warning', 'Menghapus data');
			redirect('backend/'.$this->modul);
		}
	}

	public function delete_bulk()
	{
		$id = $this->input->post('table_records');
		if (count($id) < 1) {
			$this->session->set_flashdata('alert_warning', 'Anda belum memilih data');
			redirect('backend/'.$this->modul);
		}
		for ($i=0; $i < count($id); $i++) { 
			// cek dipakai atau tidak
			$get_user = $this->wd_db->get_data('user',array('id_bagian' => $id[$i],));
			$get_bagian = $this->wd_db->get_data('bagian',array('id_bagian' => $id[$i],));
			if (count($get_user) > 0) {
				$this->session->set_flashdata('alert_warning', 'Tidak dapat menghapus data '.$get_bagian[0]['nama_bagian'].', Data masih terpakai oleh '.$get_user[0]['nama']);
				redirect('backend/'.$this->modul);
			}

			$this->db->where($this->id_name, $id[$i]);
			$qd = $this->db->delete($this->tabel);
			if (!$qd) {
				$this->session->set_flashdata('alert_warning', 'Terjadi Kesalahan');
				redirect('backend/'.$this->modul);
			}else{
				$this->create_activity($this->data_user_aktif['id_user'],'Menghapus data '.$get_bagian[0]['nama_bagian'].' pada menu bagian');
			}
		}

		if ($qd) {
			$this->session->set_flashdata('alert_success', 'Data berhasil dihapus');
			redirect('backend/'.$this->modul);
		}
	}

}

/* End of file Presensi.php */
/* Location: ./application/modules/backend/presensi/controllers/Presensi.php */