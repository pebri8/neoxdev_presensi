            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Tambah IP Router" id="tambah_ip">
                <span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
              </a>
              <style>
                #loading_add_ip{
                  /*display: none;*/
                }
              </style>
              <a id="loading_add_ip" data-toggle="tooltip" data-placement="top" title="Loading..">
                <span>
                  <img src="<?php echo base_url() ?>assets/img/loading_cat.svg" width='23px' alt="">
                </span>
              </a>
<!--               <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url() ?>backend/auth/logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a> -->
            </div>

<script>
  $("a#loading_add_ip").hide();
  $('#tambah_ip').click(function(e) {
    e.preventDefault();
    var link = this.getAttribute('link');
    var data = 'type=a';
      $.ajax({
          url: '<?php echo base_url() ?>backend/dashboard/tambah_ip_router',
          type: 'POST',
          dataType: 'json',
          data: data,
          beforeSend: function() {
              console.log(data);
              $("a#tambah_ip").hide();
              $("a#loading_add_ip").show();
          },
          success: function(data) {
            // alert(data.success)
              $("a#loading_add_ip").hide();
              $("a#tambah_ip").show();
              if(data.success){
                $.alert({
                    title: 'Success!',
                    content: data.success,
                });
              }else{
                $.alert({
                    title: 'Error!',
                    content: data.error,
                });
              }
          },
          error: function() {
              alert('error')
          }
      });
  });
</script>