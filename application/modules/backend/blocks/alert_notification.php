          <?php if ($this->session->flashdata('alert_success')): ?>
          <div id="error_login" class="alert alert-success alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('alert_success'); ?>
          </div>
          <?php endif ?>

          <?php if ($this->session->flashdata('alert_danger')): ?>
          <div id="error_login" class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <strong>Error!</strong> <?php echo $this->session->flashdata('alert_danger'); ?>
          </div>
          <?php endif ?>

          <?php if ($this->session->flashdata('alert_warning')): ?>
          <div id="error_login" class="alert alert-warning alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <strong>Warning!</strong> <?php echo $this->session->flashdata('alert_warning'); ?>
          </div>
          <?php endif ?>

          <?php if ($this->session->flashdata('alert_info')): ?>
          <div id="error_login" class="alert alert-info alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <strong>Error!</strong> <?php echo $this->session->flashdata('alert_info'); ?>
          </div>
          <?php endif ?>