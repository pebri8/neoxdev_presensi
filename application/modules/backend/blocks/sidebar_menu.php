            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">

          					<li class=""><a href="<?php echo base_url() ?>backend/dashboard"><i class="fa fa-home"></i> Dashboard </a>
          					</li>
          					<li class=""><a href="<?php echo base_url() ?>backend/bagian"><i class="fa fa-group"></i> Bagian </a>
          					</li>
                    <li class=""><a href="<?php echo base_url() ?>backend/pegawai"><i class="fa fa-user"></i> Tim </a>
                    </li>
                    <li class=""><a href="<?php echo base_url() ?>backend/presensi"><i class="fa fa-check"></i> Presensi </a>
                    </li>
                    <li class=""><a href="<?php echo base_url() ?>backend/profil"><i class="fa fa-cogs"></i> Profil </a>
                    </li>
<!--                     <li class="" id="tambah_ip"><a href="javascript:void(0)"><i class="fa fa-check"></i> Aktifkan Presensi </a>
                    </li> -->
<!--                     <li class="active"><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                          <li><a href="#level1_1">Level One</a>
                          <li><a>Level One<span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                              <li class="sub_menu"><a href="level2.html">Level Two</a>
                              </li>
                              <li><a href="#level2_1">Level Two</a>
                              </li>
                              <li><a href="#level2_2">Level Two</a>
                              </li>
                            </ul>
                          </li>
                          <li><a href="#level1_2">Level One</a>
                          </li>
                      </ul>
                    </li>  -->
                  </ul>
                </div>

              </div>