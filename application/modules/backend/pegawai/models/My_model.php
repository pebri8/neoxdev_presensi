<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_model extends CI_Model {

	var $table = 'user';

	public function hitung($object)
	{
    	$this->pagination_join_trip($object);
    	$this->pagination_where_trip($object);
    	$this->pagination_like_trip($object);
        $query = $this->db->get($this->table);
		return $query->num_rows();
	}

	public function getData($object)
	{		
		$this->pagination_like_trip($object);
		$this->pagination_join_trip($object);
		$this->pagination_where_trip($object);
		$this->db->order_by('id_user', 'desc');
		return $this->db->get($this->table, $object['limit'], $object['offset'])->result();
	}

	private function pagination_join_trip($object)
	{
		$this->db->join('bagian', 'bagian.id_bagian = user.id_bagian', 'left');
		$this->db->join('izin', 'izin.id_izin = user.id_izin', 'left');
	}

	private function pagination_where_trip($object)
	{

	}

	private function pagination_like_trip($object)
	{
		$this->db->where("(
			nama LIKE '%".$object['keyword']."%' OR
			nama LIKE '%".$object['keyword']."%'
			)", NULL, FALSE);
	}	

}

/* End of file My_model.php */
/* Location: ./application/modules/backend/presensi/models/My_model.php */