            <div class="page-title">
              <div class="title_left">
                <h3>Pegawai / Form</h3>
              </div>

            </div>

            <div class="clearfix"></div>

            <?php require_once __DIR__."/../../blocks/alert_notification.php"; ?>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Form Tambah User Pegawai <small>Silahkan diisi</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form action="<?php echo base_url() ?>backend/<?php echo $this->modul ?>/save" method="post" id="form" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" accept-charset="utf-8">
                    <input type="hidden" name="id" value="<?php echo $list ? $list['id_user'] : '' ?>">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Bagian <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select required id="select" name="id_bagian" class="form-control col-md-6 col-sm-6 col-xs-12" style="width: 100%;">
                            <option disabled selected value="AL">Pilih Bagian</option>
                            <?php foreach ($list_bagian as $dt): ?>
                              <option <?php echo ($list ? $list['id_bagian'] : '') == $dt['id_bagian'] ? ' selected' : '' ?> value="<?php echo $dt['id_bagian'] ?>"><?php echo $dt['nama_bagian']; ?></option>
                            <?php endforeach ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="nama" value="<?php echo $list ? $list['nama'] : '' ?>" class="form-control col-md-7 col-xs-12" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Email <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="email" name="email" value="<?php echo $list ? $list['email'] : '' ?>" class="form-control col-md-7 col-xs-12" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Image <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <?php if ($list): ?>
                            <img id="file_preview" src="<?php echo base_url() ?>assets/user_img/<?php echo json_decode($list['gambar']) ?>" alt="" height="100px" />
                            <input type="hidden" name="img_lama" value="<?php echo json_decode($list['gambar']) ?>">
                            <input type="file" name="file[]" id="file_input" onchange="ValidateSingleInput(this);">
                          <?php else: ?>
                            <img id="file_preview" src="#" alt="" height="100px" />
                            <input type="file" name="file[]" id="file_input" onchange="ValidateSingleInput(this);" required="">
                          <?php endif ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Password <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <?php if ($list): ?>
                            <input type="password" disabled value="<?php echo $list ? $list['password'] : '' ?>" class="form-control col-md-7 col-xs-12" required>
                            <a href="<?php echo base_url() ?>backend/<?php echo $this->modul ?>/reset_password/<?php echo $list['id_user'] ; ?>" title="Reset password" class="r_pass btn btn-xs btn-warning small">Reset Password</a>
                          <?php else: ?>
                            <input type="text" name="password" disabled value="<?php echo $rand_pass = substr(uniqid(), 10,3) ?>" class="form-control col-md-7 col-xs-12" required>
                            <input type="hidden" name="password" value="<?php echo $rand_pass; ?>">
                          <?php endif ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Device ID <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" onkeydown="return false" disabled value="<?php echo $list ? ($list['device_id']!="" ? $list['device_id'] : 'Sinkronkan dengan device!') :''?>" class="form-control col-md-7 col-xs-12" required>
                          <input type="hidden" name="device_id" value="<?php echo $list ? $list['device_id'] : ''?>">
                          <?php if ($list): ?>
                            <a href="<?php echo base_url() ?>backend/<?php echo $this->modul ?>/reset_device_id/<?php echo $list['id_user'] ; ?>" title="Reset device ID" class="r_d_id btn btn-xs btn-warning small">Reset Device ID</a>
                          <?php endif ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Tipe Izin <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select required id="select_2" name="id_izin" class="form-control col-md-6 col-sm-6 col-xs-12" style="width: 100%;">
                            <option disabled selected value="AL">Tipe Izin</option>
                            <?php foreach ($list_izin as $dt): ?>
                              <option <?php echo ($list ? $list['id_izin'] : '') == $dt['id_izin'] ? ' selected' : '' ?> value="<?php echo $dt['id_izin'] ?>"><?php echo $dt['nama_izin']; ?></option>
                            <?php endforeach ?>
                          </select>
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="submit" class="btn btn-success">Simpan</button>
						              <button class="btn btn-primary" type="reset">Reset</button>
                          <a class="btn btn-primary" href="<?php echo base_url() ?>backend/<?php echo $this->modul ?>">Kembali</a>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>

	<script>
	$('#ddd').datepicker({
	    range: 'true',
	    language: 'en',
	    maxDate: new Date()
	});
	</script>

	<script>
	$(document).ready(function() {
      $('#select').select2();
	    $('#select_2').select2();
	});
	</script>
  
  <script>
  $('a.r_d_id').confirm({
      title: 'Confirm?',
      content: 'Anda yakin akan me-reset device id?',
      buttons: {
          confirm: function () {
              location.href = this.$target.attr('href');
          },
          cancel: function () {
          },
      }
  });
  </script>

<script>
  $('a.r_pass').confirm({
      title: 'Confirm?',
      content: 'Anda yakin akan me-reset password user yang dipilih?',
      buttons: {
          confirm: function () {
              location.href = this.$target.attr('href');
          },
          cancel: function () {
          },
      }
  });
</script>
<?php if ($this->session->flashdata('jc_r_s')): ?>
<script>
  $.alert({
      title: 'Success!',
      content: '<?php echo $this->session->flashdata('jc_r_s'); ?>',
  });
</script>
<?php endif ?>

<!-- Validate input type file -->
<script>
var _validFileExtensions = [".jpg", ".jpeg", ".gif", ".png"];
function ValidateSingleInput(oInput) {
    if (oInput.type == "file") {
        var sFileName = oInput.value;
         if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }
             
            if (!blnValid) {
                // alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                document.getElementById("file_preview").style.display="none";
                $.alert({
                    title: 'File Not Allowed Extensions!',
                    content: 'Allowed extensions are '+ _validFileExtensions.join(", "),
                });
                oInput.value = "";
                return false;
            }
        }
    }
    return true;
}
</script>

<script>
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      document.getElementById("file_preview").style.display="block";
      $('#file_preview').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#file_input").change(function() {
  readURL(this);
});
</script>