		<table width="700" align="center" border="0" cellpadding="0" cellspacing="0">
			<tr><th colspan="3" class="tl">LAPORAN PRESENSI</th></tr>
			<tr>
				<td width="90">Dari bulan</td><td width="12">:</td>
				<td><?php echo isset($d_r[0]) ? $d_r[0] : '-' ; ?></td>
			</tr>
			<tr>
				<td>Sampai</td><td>:</td>
				<td><?php echo isset($d_r[1]) ? $d_r[1] : '-' ; ?></td>
			</tr>
		</table>
        <br>
        <table width="700" align="center" border="0" cellpadding="0" cellspacing="0" class="krs_box">
            <tbody>
                <tr>
                    <th width="10%">NO</th>
                    <th width="30%">NAMA</th>
                    <th width="20%">MASUK</th>
                    <th width="20%">PULANG</th>
                    <th width="20%">KETERANGAN</th>
                </tr>
                <?php $selislih = 0; ?>
                <?php $no = 1; ?>
                <?php foreach ($list_data as $dt): ?>
                <tr>
                    <td><?php echo $no; $no++; ?></td>
                    <td class="td"><?php echo $dt->nama ?></td>
                    <td><?php echo $dt->masuk ?></td>
                    <td><?php echo $dt->pulang ?></td>
                    <td><?php echo $dt->status_presensi == '1' ? 'Complete' : 'Jam pulang kosong' ?></td>

                    <!-- sum hours -->
                    <?php 
                        $h_masuk = explode(' ', $dt->masuk);
                        $h_masuk = explode(':', $h_masuk[1]);
                        $hr_m_fix = ($h_masuk[0]*60)+$h_masuk[1];

                        $h_pulang = explode(' ', $dt->pulang);
                        $h_pulang = explode(':', $h_pulang[1]);
                        $hr_p_fix = ($h_pulang[0]*60)+$h_pulang[1];

                        $selislih = $selislih + $hr_p_fix - $hr_m_fix;

                        $t_hr = $selislih;
                        $selisih_fix = floor($t_hr/60)." hr ".($t_hr%60)." min";
                    ?>
                </tr>
                <?php endforeach ?>
                <tr>
                    <th class="tr" colspan="4">Total Jam Kerja</th>
                    <th class="tc" colspan="2"><?php echo $selisih_fix; ?></th>
                </tr>
            </tbody>
        </table>