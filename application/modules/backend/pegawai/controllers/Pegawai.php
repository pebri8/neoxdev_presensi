<?php
defined('BASEPATH') OR exit('No direct script access allowed'); require 'application/modules/backend/grab/controllers/Grab.php';

class Pegawai extends Grab {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('My_model');
		$this->modul = "pegawai";
		$this->tabel = 'user';
		$this->id_name = 'id_user';

		$this->file_allowed_types = 'gif|jpg|png|jpeg';
		$this->folder_file = 'assets/user_img';
		$this->folder_thumb_file = 'assets/user_img/thumb';

		// resize file
		// $this->resize = '';
		$this->resize_width = '75';
		$this->resize_height = '50';
	}

	public function index()
	{
        $this->load->library('pagination');
        $params['v'] = 'search';

        if ($this->input->get('keyword') == true) {
            $params['keyword'] = $this->input->get('keyword');
        }
        if ($this->input->get('range_date') == true) {
            $params['range_date'] = $this->input->get('range_date');
            $range_date = explode(' - ', $params['range_date']);
            // debug($aa);
        }
        if ($this->input->get('per_page') == true) {
            $params['per_page'] = $this->input->get('per_page');
        }

        $queryString =  http_build_query($params);
        $hpp = explode('&per_page', $queryString);

        $config['base_url'] = base_url().'backend/'.$this->modul.'?'.$hpp[0];
        $config['per_page'] = 5;
        $config['uri_segment'] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        // $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        // $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';        
        $config['last_link'] = 'Last';        
        $config['first_tag_open'] = '<li>';        
        $config['first_tag_close'] = '</li>';        
        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li>';      
        $config['prev_tag_close'] = '</li>';          
        $config['next_link'] = 'Next';        
        $config['next_tag_open'] = '<li>';        
        $config['next_tag_close'] = '</li>';        
        $config['last_tag_open'] = '<li>';        
        $config['last_tag_close'] = '</li>';        
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void()">';        
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';        
        $config['num_tag_close'] = '</li>';

        $offset = 0;
        if (!empty($_GET['per_page'])) {
            $pageNo = $_GET['per_page'];
            $offset = ($pageNo - 1) * $config["per_page"];
        }

        $object = array(
            'limit' => $config['per_page'],
            'offset' => $offset,
            'keyword' => $this->input->get('keyword'),
            'd_r' => isset($range_date) ? $range_date : '',
        );

        $config['total_rows'] = $this->My_model->hitung($object);
        $data['no_urut'] = $offset+1;
        $this->pagination->initialize($config);

        $data['total_rows'] = $this->My_model->hitung($object);
        $data['list_data'] = $this->My_model->getData($object);
        // debug($data['total_rows']);

        $data['content'] = "index";
		$this->view($data,false);
	}

	public function form($id=null)
	{
		$data['list_bagian'] = $this->My_model_main->bagian();
		$data['list_izin'] = $this->My_model_main->izin();
		$data['list'] = $this->wd_db->get_data_row($this->tabel, array($this->id_name => $id,));
		$data['content'] = "form";
		$this->view($data);
	}

	public function save()
	{
		$id = $this->input->post('id');
		$id_bagian = $this->input->post('id_bagian');
		$nama = $this->input->post('nama');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$id_izin = $this->input->post('id_izin');

		$id_back = $id ? '/'.$id : '';
		$error_link = 'backend/'.$this->modul.'/form'.$id_back;
		$up = $this->upload_single($error_link);


		if ($up) {
			// echo json_encode($up);
			if ($id) {
				$ob = array(
					'id_bagian' => $id_bagian,
					'nama' => $nama,
					'email' => $email,
					'gambar' => json_encode($up),
					'id_izin' => $id_izin,
				);
				$this->db->where($this->id_name, $id);
				$qi = $this->db->update($this->tabel, $ob);
			}else{
				$ob = array(
					'id_bagian' => $id_bagian,
					'nama' => $nama,
					'email' => $email,
					'gambar' => json_encode($up),
					'password' => md5($password),
					'id_izin' => $id_izin,
				);
				$qi = $this->db->insert($this->tabel, $ob);
			}
			if ($qi) {
				if ($id) {
					$this->session->set_flashdata('alert_success', 'Data berhasil diedit');
					redirect('backend/'.$this->modul.'/form/'.$id.'');
				}else{
					$this->session->set_flashdata('alert_success', 'Data berhasil ditambahkan');
					$this->create_activity($this->data_user_aktif['id_user'],'Menambah data '.$nama.' pada menu karyawan');
					redirect('backend/'.$this->modul.'/form');
				}
			}else{
				if ($id) {
					$this->session->set_flashdata('alert_warning', 'Edit data');
					redirect('backend/'.$this->modul.'/form/'.$id.'');
				}else{
					$this->session->set_flashdata('alert_warning', 'Menambah data');
					redirect('backend/'.$this->modul.'/form');
				}
			}
		}

	}

	public function delete($id)
	{
		$get_pegawai = $this->wd_db->get_data_row('user',array('id_user' => $id));
		$gambar = json_decode($get_pegawai['gambar']);

		$this->db->where($this->id_name, $id);
		$qd = $this->db->delete($this->tabel);

		$this->folder_file = 'assets/user_img';
		$this->folder_thumb_file = 'assets/user_img/thumb';

		if ($qd) {
			$this->session->set_flashdata('alert_success', 'Data berhasil dihapus');

			// hapus data presensi
			$this->db->where('id_user', $id);
			$qdp = $this->db->delete('presensi');
			if ($qdp) {
        		@unlink('./'.$this->folder_file.'/'.$gambar);
        		@unlink('./'.$this->folder_thumb_file.'/thumb_'.$gambar);
        		$this->create_activity($this->data_user_aktif['id_user'],'Menghapus data '.$get_pegawai['nama'].' pada menu karyawan');
        		$this->wd_db->del_dml('aktivitas',array('id_user' => $id, ));
				redirect('backend/'.$this->modul);
			}
		}else{
			$this->session->set_flashdata('alert_warning', 'Menghapus data');
			redirect('backend/'.$this->modul);
		}
	}

	public function delete_bulk()
	{
		$id = $this->input->post('table_records');
		if (count($id) < 1) {
			$this->session->set_flashdata('alert_warning', 'Anda belum memilih data');
			redirect('backend/'.$this->modul);
		}
		for ($i=0; $i < count($id); $i++) { 

			$get_pegawai = $this->wd_db->get_data_row('user',array('id_user' => $id[$i]));
			$gambar = json_decode($get_pegawai['gambar']);

			$this->db->where($this->id_name, $id[$i]);
			$qd = $this->db->delete($this->tabel);
			if (!$qd) {
				$this->session->set_flashdata('alert_warning', 'Terjadi Kesalahan');
				redirect('backend/'.$this->modul);
			}else{
				$this->wd_db->del_dml('aktivitas',array('id_user' => $id[$i], ));
				$this->create_activity($this->data_user_aktif['id_user'],'Menghapus data '.$get_pegawai['nama'].' pada menu karyawan');
			}

			// hapus data presensi
			$this->db->where('id_user', $id[$i]);
			$qdp = $this->db->delete('presensi');
    		@unlink('./'.$this->folder_file.'/'.$gambar);
    		@unlink('./'.$this->folder_thumb_file.'/thumb_'.$gambar);
		}

		if ($qd) {
			$this->session->set_flashdata('alert_success', 'Data berhasil dihapus');
			redirect('backend/'.$this->modul);
		}
	}

	function reset_device_id($id=null)
	{
		$ob = array(
			'status_login' => '0',
			'device_id' => '',
		);
		$get_pegawai = $this->wd_db->get_data_row('user',array('id_user' => $id));
		$this->db->where($this->id_name, $id);
		$qu = $this->db->update($this->tabel, $ob);
		if ($qu) {
			$this->session->set_flashdata('alert_success', 'Device ID berhasil di reset');
			$this->create_activity($this->data_user_aktif['id_user'],'Reset device id pada karyawan '.$get_pegawai['nama'].'');
			redirect('backend/'.$this->modul.'/form/'.$id.'');
		}
	}

	function reset_password($id=null)
	{
		$get_pegawai = $this->wd_db->get_data_row('user',array('id_user' => $id));
		$rand_pass = substr(uniqid(), 10,3);
		$ob = array('password' => md5($rand_pass),);
		$this->db->where($this->id_name, $id);
		$qu = $this->db->update($this->tabel, $ob);
		if ($qu) {
			$this->session->set_flashdata('alert_success', 'Password berhasil di reset');
			$this->session->set_flashdata('jc_r_s', 'Password baru : <b>'.$rand_pass.'</b>');
			$this->create_activity($this->data_user_aktif['id_user'],'Reset password pada karyawan '.$get_pegawai['nama'].'');
			redirect('backend/'.$this->modul.'/form/'.$id.'');
		}
	}

	public function report()
	{
        $params['v'] = 'search';

        if ($this->input->get('keyword') == true) {
            $params['keyword'] = $this->input->get('keyword');
        }
        if ($this->input->get('range_date') == true) {
            $params['range_date'] = $this->input->get('range_date');
            $range_date = explode(' - ', $params['range_date']);
            // debug($aa);
        }
        if ($this->input->get('per_page') == true) {
            $params['per_page'] = $this->input->get('per_page');
        }
        $object = array(
            'limit' => 999999,
            'offset' => 0,
            'keyword' => $this->input->get('keyword'),
            'd_r' => isset($range_date) ? $range_date : '',
        );

        $data['list_data'] = $this->My_model->getData($object);
        $data['d_r'] = isset($range_date) ? $range_date : '';
        // debug($data['d_r']);

		$data['content'] = 'report';
		$this->report_template($data);
	}

}

/* End of file Presensi.php */
/* Location: ./application/modules/backend/presensi/controllers/Presensi.php */