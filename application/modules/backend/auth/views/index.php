<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo base_url() ?>assets/img/logo_neox.svg" type="image/ico/svg" />

    <title>Feeltrip Presensi</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url() ?>assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url() ?>assets/admin/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url() ?>assets/admin/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo base_url() ?>assets/admin/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url() ?>assets/admin/build/css/custom.min.css" rel="stylesheet">

    <script src="<?php echo base_url() ?>assets/admin/vendors/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form id="form_login" action="<?php echo base_url() ?>backend/auth/login" method="post">
              <h1>ADMINISTRATOR</h1>

                <div id="load_error">
              <?php if ($this->session->flashdata('error_danger')): ?>
                  <div id="error_login" class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <strong>Error!</strong> <?php echo $this->session->flashdata('error_danger'); ?>.
                  </div>
              <?php endif ?>
                </div>

              <div>
                <input type="email" name="email" class="form-control" placeholder="Email" required="" />
              </div>
              <div>
                <input type="password" name="password" class="form-control" placeholder="Kata Sandi" required="" />
              </div>
              <style>
.btn_login:hover {
    color: white;
    background-color: #8ec63f;
}
.btn_login {
    color: white;
    background-color: #428bca;
    padding: 8px 50px;
    width: 100%;
    border: none;
        border-top-color: currentcolor;
        border-right-color: currentcolor;
        border-bottom-color: currentcolor;
        border-left-color: currentcolor;
}
              </style>
              <div>
                <button class="btn_login btn btn-default submit" type="submit">Masuk</button>
              </div>
              <br>
              <!-- <a class="reset_pass" href="<?php echo base_url() ?>auth/reset">Ganti kata sandi?</a> -->

              <div class="clearfix"></div>

              <div class="separator">

                <div class="clearfix"></div>
                <br />

                <div>
                  <img src="<?php echo base_url() ?>assets/img/logo_neox.svg" height="50px">
                  <br>
                  <br>
                  <p>©2018 Hak Cipta. Neox Indonesia.</p>
                </div>
              </div>
            </form>
          </section>
        </div>

      </div>
    </div>
  </body>
    <script>
      $('#form_login').submit(function (e) {
      e.preventDefault();
      var frm = $('#form_login');
          var data = $("#form_login").serialize();
          var url = $(this).attr('action');
          var type = $(this).attr('method');
          $.ajax({
              url: url,
              type: type,
              dataType: 'JSON',
              data: data,
              beforeSend: function() {
                  console.log(data);
              },
              success: function(data) {
                  if(data.success){
                    location.reload();
                  }else{
                    $('#form_login')[0].reset();
                    $('#load_error').load(location + ' #error_login');
                  }
              },
              error: function() {
                  alert('error')
              }
          });
      });
    </script>

</html>