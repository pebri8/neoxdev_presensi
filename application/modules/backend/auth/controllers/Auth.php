<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('My_model');
	}

	public function index()
	{
		// session_destroy();
		// debug($this->session->userdata());
		if ($this->session->userdata('admin_login')) {
			redirect('backend/dashboard');
		}
		$this->load->view('index');
	}

	function cs()
	{
		debug($this->session->userdata());
	}

	public function forgot()
	{
		$this->load->view('lupa_kata_sandi');
	}

	public function login2()
	{
		$res = array('success' => 's', );
		echo json_encode($res);
	}

	public function login()
	{
		$data = array(
			'email' => $this->input->post('email', TRUE),
			'password' => md5($this->input->post('password', TRUE)),
			'id_izin' => '1',
		);

		$hasil = $this->My_model->cek_user($data);
		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['admin_login'] = 'ok';
				// $sess_data['id_user_admin'] = $sess->id_user;
				$sess_data['nama'] = $sess->nama;
				$sess_data['id_user'] = $sess->id_user;
				// Set session
				$this->session->set_userdata($sess_data);
				// $this->create_activity($sess->id_user,'Login admin presensi');
			}
			$res = array('success' => 's', );
		}
		else {
			$this->session->set_flashdata('error_danger', 'Email atau password tidak cocok!');
			$res = array('error' => 'e', );
		}
		echo json_encode($res);
	}

	public function logout()
	{
		session_destroy();
		redirect('backend/auth');
	}

}

/* End of file Auth.php */
/* Location: ./application/modules/backend/auth/controllers/Auth.php */