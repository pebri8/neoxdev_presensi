                          <div role="tabpanel" class="tab-pane fade active in" id="tab_aktivitas" aria-labelledby="home-tab">

                            <!-- start recent activity -->
                            <ul class="messages">
                              <?php

                              $list_aktivitas = $this->My_model_main->get_aktivitas('5',array('aktivitas.id_user' => $this->data_user_aktif['id_user']));


                              ?>
                              <?php foreach ($list_aktivitas as $key): ?>
                              <li>
                                <img src="<?php echo base_url() ?>assets/user_img/thumb/thumb_<?php echo json_decode($this->data_user_aktif['gambar']) ?>" class="avatar" alt="Avatar">
                                <div class="message_date">
                                  <h3 class="date text-info"><?php echo date("d", strtotime($key['tanggal']) ); ?></h3>
                                  <p class="month"><?php echo date("F", strtotime($key['tanggal']) ); ?></p>
                                </div>
                                <div class="message_wrapper">
                                  <h4 class="heading"><?php echo $key['nama'] ?></h4>
                                  <blockquote class="message"><?php echo $key['keterangan'] ?></blockquote>
                                  <br />
                                </div>
                              </li>
                              <?php endforeach ?>

                            </ul>
                            <!-- end recent activity -->

                          </div>