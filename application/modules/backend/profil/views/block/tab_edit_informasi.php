                          <style>
                          	.custom_label{
                          		text-align: right;
                          		padding: 5px;
                          	}
                          </style>
                          <div role="tabpanel" class="tab-pane fade" id="tab_edit_informasi" aria-labelledby="profile-tab">

                          	<form id="f_i_pribadi" action="<?php echo base_url() ?>backend/profil/save" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                          		<input type="hidden" name="id" value="<?php echo $list_user['id_user'] ?>">
		                      <div class="form-group">
		                        <label class="custom_label control-label col-md-3 col-sm-3 col-xs-12">Nama <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                          <input type="text" name="nama" value="<?php echo $list_user['nama'] ?>" class="form-control col-md-7 col-xs-12" required>
		                        </div>
		                      </div>
							  <div class="clearfix"></div>
		                      <br>

		                      <div class="form-group">
		                        <label class="custom_label control-label col-md-3 col-sm-3 col-xs-12">Email <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                          <input type="email" name="email" value="<?php echo $list_user['email'] ?>" class="form-control col-md-7 col-xs-12" required>
		                        </div>
		                      </div>
							  <div class="clearfix"></div>
		                      <br>

		                      <div class="form-group">
		                        <label class="custom_label control-label col-md-3 col-sm-3 col-xs-12">Gambar <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                        	<input type="hidden" name="img_lama" value="<?php echo json_decode($list_user['gambar']) ?>">
		                        	<img id="file_preview" src="<?php echo base_url() ?>assets/user_img/<?php echo json_decode($list_user['gambar']) ?>" alt="" height="100px" />
		                          	<input type="file" name="file[]" id="file_input" onchange="ValidateSingleInput(this);">
		                        </div>
		                      </div>
							  <div class="clearfix"></div>
		                      <br>

		                      <div class="form-group">
		                        <label class="custom_label control-label col-md-3 col-sm-3 col-xs-12">Kata Sandi <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                          <input type="password" name="password" value="" class="form-control col-md-7 col-xs-12" required>
		                        </div>
		                      </div>
							  <div class="clearfix"></div>
		                      <br>

		                      <div class="form-group">
		                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
		                          <button type="submit" class="btn btn-success">Simpan</button>
								  <!-- <button class="btn btn-primary" id="reset_form_informasi">Reset</button> -->
		                        </div>
		                      </div>
							  <div class="clearfix"></div>
		                      <br>
                          	</form>

                          </div>

<!-- <script>
	$('button#reset_form_informasi').click(function(){
		alert('asd')
		$('#tab_edit_informasi').load(location + ' #f_i_pribadi');
		// $('#load_error').load(location + ' #error_login');
	});
</script>
 -->
<!-- Validate input type file -->
<script>
var _validFileExtensions = [".jpg", ".jpeg", ".gif", ".png"];
function ValidateSingleInput(oInput) {
    if (oInput.type == "file") {
        var sFileName = oInput.value;
         if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }
             
            if (!blnValid) {
                // alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                document.getElementById("file_preview").style.display="none";
                $.alert({
                    title: 'File Not Allowed Extensions!',
                    content: 'Allowed extensions are '+ _validFileExtensions.join(", "),
                });
                oInput.value = "";
                return false;
            }
        }
    }
    return true;
}
</script>

<script>
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      document.getElementById("file_preview").style.display="block";
      $('#file_preview').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#file_input").change(function() {
  readURL(this);
});
</script>