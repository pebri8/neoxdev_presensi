                          <style>
                          	.custom_label{
                          		text-align: right;
                          		padding: 5px;
                          	}
                          </style>
                          <div role="tabpanel" class="tab-pane fade" id="tab_ubah_password" aria-labelledby="profile-tab">

                          	<form id="f_i_pribadi" action="<?php echo base_url() ?>backend/profil/ubah_kata_sandi" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                          		<input type="hidden" name="id" value="<?php echo $list_user['id_user'] ?>">

		                      <div class="form-group">
		                        <label class="custom_label control-label col-md-3 col-sm-3 col-xs-12">Email <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                          <input type="email" name="email" value="<?php echo $list_user['email'] ?>" class="form-control col-md-7 col-xs-12" required>
		                        </div>
		                      </div>
							  <div class="clearfix"></div>
		                      <br>

		                      <div class="form-group">
		                        <label class="custom_label control-label col-md-3 col-sm-3 col-xs-12">Kata Sandi <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                          <input type="password" name="password" value="" class="form-control col-md-7 col-xs-12" required>
		                        </div>
		                      </div>
							  <div class="clearfix"></div>
		                      <br>

		                      <div class="form-group">
		                        <label class="custom_label control-label col-md-3 col-sm-3 col-xs-12">Kata Sandi Baru <span class="required">*</span>
		                        </label>
		                        <div class="col-md-6 col-sm-6 col-xs-12">
		                          <input type="password" name="password_new" value="" class="form-control col-md-7 col-xs-12" required>
		                        </div>
		                      </div>
							  <div class="clearfix"></div>
		                      <br>

		                      <div class="form-group">
		                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
		                          <button type="submit" class="btn btn-success">Simpan</button>
								  <!-- <button class="btn btn-primary" id="reset_form_informasi">Reset</button> -->
		                        </div>
		                      </div>
							  <div class="clearfix"></div>
		                      <br>
                          	</form>

                          </div>