            <div class="page-title">
              <div class="title_left">
                <h3>User Profil</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
<!--                     <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Cari!</button>
                    </span> -->
                  </div>
                </div>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Ringkasan Profil <small>Ringkasan Aktivias</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img class="img-responsive avatar-view" width="100%" src="<?php echo base_url() ?>assets/user_img/<?php echo json_decode($this->data_user_aktif['gambar']) ?>" alt="Avatar" title="Change the avatar">
                        </div>
                      </div>
                      <h3><?php echo $this->data_user_aktif['nama']; ?></h3>

                      <a class="get_popup btn btn-success"><i class="fa fa-edit m-right-xs"></i>Edit Profil</a>
                      <br />

                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class=""><a href="#tab_aktivitas" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Ringkasan Aktivitas</a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_edit_informasi" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Informasi Pribadi</a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_ubah_password" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Ubah Kata Sandi</a>
                          </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <?php require 'block/tab_aktivitas.php'; ?>
                          <?php require 'block/tab_edit_informasi.php'; ?>
                          <?php require 'block/tab_ubah_password.php'; ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- <button id="open_pop_up" link="test">a</button> -->

<!-- load pop up -->
<div class="load_popup">
  <div id="yes" class="load_popupq">
    <?php if ($this->session->flashdata('fl_popup')): ?>
    <?php $this->load->view('block/pop_edit_profil'); ?>
    <?php endif ?>
  </div>
</div>

<script>
    $('body').on('click', '#close_pop_up', function () {
      $('.load_popup').load(location + ' .load_popupq');
    });
</script>

<!-- <button class="get_popup" page="block/pop_edit_profil">Get</button> -->

<script>
  $('.get_popup').click(function(e) {
    e.preventDefault();
    var link = this.getAttribute('link');
    var page = this.getAttribute('page');
    var data = 'type='+link+'&page='+page;
    // var data = 'type=asd';
      $.ajax({
          url: '<?php echo base_url() ?>api/load/page',
          type: 'POST',
          dataType: 'json',
          data: data,
          beforeSend: function() {
              console.log(data);
          },
          success: function(data) {
            // alert(data.success)
              // if(data.success){
                $('.load_popup').load(location + ' .load_popupq');
               // document.getElementById('yes').innerHTML='<button type="">asdasd</button><script>alert("asd")<script/>';
              // }else{
              //   alert('error');
              // }
          },
          error: function() {
              alert('error')
          }
      });
  });
</script>

<?php if ($this->session->flashdata('alert_error')): ?>
<script>
  $.alert({
      title: 'Error!',
      content: '<?php echo $this->session->flashdata('alert_error') ?>',
  });
</script>
<?php endif ?>

<?php if ($this->session->flashdata('alert_success')): ?>
<script>
  $.alert({
      title: 'Success!',
      content: '<?php echo $this->session->flashdata('alert_success') ?>',
  });
</script>
<?php endif ?>