<?php
defined('BASEPATH') OR exit('No direct script access allowed'); require 'application/modules/backend/grab/controllers/Grab.php';

class Profil extends Grab {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('My_model');

		$this->modul = "profil";
		$this->tabel = 'user';
		$this->id_name = 'id_user';

		$this->file_allowed_types = 'gif|jpg|png|jpeg';
		$this->folder_file = 'assets/user_img';
		$this->folder_thumb_file = 'assets/user_img/thumb';

		// resize file
		// $this->resize = '';
		$this->resize_width = '75';
		$this->resize_height = '50';
	}

	public function index()
	{

		$data['list_presensi'] = $this->My_model_main->presensi($limit=4);

		// $data['list_presensi_today'] = $this->My_model_main->presensi_range(date("Y-m-d"),date("Y-m-d"));
		// $data['list_presensi_yesterday'] = $this->My_model_main->presensi_range(date("Y-m-d",strtotime("-1 day")),date("Y-m-d",strtotime("-1 day")));
		// $data['selisih_presensi_today'] = count($data['list_presensi_yesterday'])-count($data['list_presensi_today']);

		// $lpty = $this->My_model_main->presensi_sum_time(date("Y-m-d",strtotime("-1 day")),date("Y-m-d",strtotime("-1 day")));
		// $lptd = $this->My_model_main->presensi_sum_time(date("Y-m-d"),date("Y-m-d"));
		// $data['list_presensi_time_today'] = floor($lptd/60)."h ".($lptd%60)."m";


		$data['list_presensi_time_yesterday'] = $this->My_model_main->presensi_sum_time(date("Y-m-d",strtotime("-1 day")),date("Y-m-d",strtotime("-1 day")));
		$data['list_presensi_month'] = $this->My_model_main->presensi_range(date("Y-m-01"),date("Y-m-t"));
		$data['list_presensi_month_yesterday'] = $this->My_model_main->presensi_range(date("Y-m-01",strtotime("-1 month")),date("Y-m-t",strtotime("-1 month")));
		$data['selisih_presensi_month'] = count($data['list_presensi_month'])-count($data['list_presensi_month_yesterday']);

		$lptmy = $this->My_model_main->presensi_sum_time(date("Y-m-01",strtotime("-1 month")),date("Y-m-t",strtotime("-1 month")));
		$lptm = $this->My_model_main->presensi_sum_time(date("Y-m-01"),date("Y-m-t"));
		$data['list_presensi_time_month'] = floor($lptm/60)."h ".($lptm%60)."m";
		$data['list_presensi_time_month_yesterday'] = $this->My_model_main->presensi_sum_time(date("Y-m-01",strtotime("-1 month")),date("Y-m-t",strtotime("-1 month")));
		$data['selisih_presensi_time_month'] = $lptm-$lptmy;

		$data['list_users'] = $this->My_model_main->karyawan();
		// debug($data['list_user']);
		$data['list_divisi'] = $this->My_model_main->bagian();

		// chart harian presensi
		$min_date = 7;
		for ($i=0; $i < 7 ; $i++) {
			$new_date = date("Y-m-d",strtotime("-".$min_date." day"));
			$min_date--;
			$list_date[$i] = $new_date;

			$selisih_presensi[$i] = count($this->My_model_main->presensi_range($new_date,$new_date));
		}
		$data['list_date_chart'] = json_encode($list_date);
		$data['list_selisih_presensi_harian'] = json_encode($selisih_presensi);
		// debug(json_encode($list_date));
		// debug($selisih_presensi);

        $data['content'] = "index";
        $this->view($data,false);
	}

	public function save()
	{
		$id = $this->data_user_aktif['id_user'];
		$nama = $this->input->post('nama');
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$cek_akun = $this->My_model->cek_user(array('id_user' => $id, 'password' => md5($password),));
		if (count($cek_akun) > 0) {
			// Edit profil
			$error_link = 'backend/'.$this->modul.'#tab_edit_informasi';
			$up = $this->upload_single($error_link);

			if ($up) {
				$ob = array(
					'nama' => $nama,
					'email' => $email,
					'gambar' => json_encode($up),
				);
				$this->db->where($this->id_name, $id);
				$qi = $this->db->update($this->tabel, $ob);
				if ($qi) {
					$this->session->set_flashdata('alert_success', 'Data Berhasil Diubah');
					$this->create_activity($id,'Mengubah Informasi Pribadi');
					redirect('backend/'.$this->modul.'#tab_edit_informasi');
				}else{
					$this->session->set_flashdata('alert_error', 'Edit data');
					redirect('backend/'.$this->modul.'#tab_edit_informasi');
				}
			}
		}else{
			$this->session->set_flashdata('alert_error', 'Password salah');
			redirect('backend/'.$this->modul.'#tab_edit_informasi');
		}
	}

	public function ubah_kata_sandi()
	{
		$id = $this->input->post('id');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$password_new = $this->input->post('password_new');

		$cek_akun = $this->My_model->cek_user(array('email' => $email, 'password' => md5($password),));
		if (count($cek_akun) > 0) {
			$this->db->where('id_user', $id);
			$ob = array(
				'password' => md5($password_new), 
			);
			$qu = $this->db->update('user', $ob);
			if ($qu) {
				$this->session->set_flashdata('alert_success', 'Kata Sandi Berhasil Diubah');
				$this->create_activity($id,'Edit Kata Sandi');
				session_destroy();
				redirect('backend/'.$this->modul.'#tab_ubah_password');
			}
		}else{
			$this->session->set_flashdata('alert_error', 'Password salah');
			redirect('backend/'.$this->modul.'#tab_ubah_password');
		}
	}

	function tes(){
		$a = $this->create_activity('1','jangan menyerah, berfikir positif peb!, pasti berhasil');
		debug($a);
	}

}

/* End of file Dashboard.php */
/* Location: ./application/modules/backend/dashboard/controllers/Dashboard.php */