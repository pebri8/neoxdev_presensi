  <script src="<?php echo base_url() ?>assets/zingchart/zingchart.min.js"></script>
<!--   <script>
    zingchart.MODULESDIR = "https://cdn.zingchart.com/modules/";
    ZC.LICENSE = ["569d52cefae586f634c54f86dc99e6a9", "ee6b7db5b51705a13dc2339db3edaf6d"];
  </script> -->

            <div class="row top_tiles">
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-long-arrow-<?php echo $selisih_presensi_month < 0 ? 'down' : 'up' ?>"></i></div>
                  <div class="count"><?php echo count($list_presensi_month); ?></div>
                  <h3>Total Presensi</h3>
                  <p>
                    <i class="<?php echo $selisih_presensi_month < 0 ? 'red' : 'green' ?>">
                      <i class="fa fa-sort-<?php echo $selisih_presensi_month < 0 ? 'desc' : 'asc' ?>"></i> <?php echo $selisih_presensi_month ?>
                    </i>
                    Dari bulan lalu
                  </p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-long-arrow-<?php echo $selisih_total_jam_kerja < 0 ? 'down' : 'up' ?>"></i></div>
                  <div class="count"><?php echo $sum_total_jam_kerja_bulan_ini['total_jam_kerja_fix']; ?></div>
                  <h3>Total Jam Kerja</h3>
                  <p>
                    <i class="<?php echo $selisih_total_jam_kerja < 0 ? 'red' : 'green' ?>">
                      <i class="fa fa-sort-<?php echo $selisih_total_jam_kerja < 0 ? 'desc' : 'asc' ?>"></i> <?php echo $selisih_total_jam_kerja_fix ?>
                    </i>
                    Dari bulan lalu
                  </p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-check-square-o"></i></div>
                  <div class="count"><?php echo count($list_users); ?></div>
                  <h3>Total Users</h3>
                  <p>Keseluruhan</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-check-square-o"></i></div>
                  <div class="count"><?php echo count($list_divisi); ?></div>
                  <h3>Total Divisi</h3>
                  <p>Keseluruhan</p>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Ringkasan Presensi <small>Progress mingguan</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" style="height:350px">
                      <style>
                        #chart_presensi  {
                          height: 100%;
                          width: 100%;
                        }
                      </style>
                      <div id='chart_presensi'></div>
                  </div>
                </div>
              </div>
            </div>

          <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Aktivitas Presensi <small>Terakhir</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="dashboard-widget-content">

                    <ul class="list-unstyled timeline widget">
                      <?php foreach ($list_presensi as $key): ?>
                      <li>
                        <div class="block">
                          <div class="block_content">
                            <h2 class="title">
                                <a><?php echo $key['nama'] ?></a>
                            </h2>
                            <div class="byline">
                              <span>Presensi tanggal : <?php echo $key['masuk'] ?></span>
                            </div>
                            <p class="excerpt"><?php echo $key['status_presensi'] == 1 ? 'Completed' : 'Jam pulang kosong' ?></a>
                            </p>
                          </div>
                        </div>
                      </li>
                      <?php endforeach ?>

                    </ul>
                  </div>
                </div>
              </div>
            </div>


            <div class="col-md-8 col-sm-8 col-xs-12">
              <div class="row">
                <!-- Start to do list -->
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>Pegawai <small>Ringkasan pegawai</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                          <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                          </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                        <ul class="list-unstyled top_profiles scroll-view">
                          <?php foreach ($list_users_limit as $key): ?>
                          <li class="media event">
                            <a class="cover_img pull-left profile_thumb" style="background-image: url(<?php echo base_url() ?>assets/user_img/thumb/thumb_<?php echo json_decode($key['gambar']) ?>);">
                              
                              <!-- <img src="" alt=""> -->
                            </a>
                            <div class="media-body">
                              <a class="title" href="#"><?php echo $key['nama'] ?></a>
                              <p><strong>Divisi: </strong> <?php echo $key['nama_bagian'] ?> </p>
                              <?php $data = $this->wd_db->get_data('presensi',array('id_user' => $key['id_user'])); ?>
                              <p> <small>Masuk Terakhir : <?php echo count($data) < 1 ? '--:--:--' : $data[0]['masuk'] ; ?></small>
                              </p>
                            </div>
                          </li>
                          <?php endforeach ?>

                        </ul>
                  </div>
                </div>
                <!-- End to do list -->
                
                <!-- start of weather widget -->
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>Daily active users <small>Sessions</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                          <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                          </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="temperature"><b>Monday</b>, 07:30 AM
                            <span>F</span>
                            <span><b>C</b></span>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="weather-icon">
                            <canvas height="84" width="84" id="partly-cloudy-day"></canvas>
                          </div>
                        </div>
                        <div class="col-sm-8">
                          <div class="weather-text">
                            <h2>Texas <br><i>Partly Cloudy Day</i></h2>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="weather-text pull-right">
                          <h3 class="degrees">23</h3>
                        </div>
                      </div>

                      <div class="clearfix"></div>

                      <div class="row weather-days">
                        <div class="col-sm-2">
                          <div class="daily-weather">
                            <h2 class="day">Mon</h2>
                            <h3 class="degrees">25</h3>
                            <canvas id="clear-day" width="32" height="32"></canvas>
                            <h5>15 <i>km/h</i></h5>
                          </div>
                        </div>
                        <div class="col-sm-2">
                          <div class="daily-weather">
                            <h2 class="day">Tue</h2>
                            <h3 class="degrees">25</h3>
                            <canvas height="32" width="32" id="rain"></canvas>
                            <h5>12 <i>km/h</i></h5>
                          </div>
                        </div>
                        <div class="col-sm-2">
                          <div class="daily-weather">
                            <h2 class="day">Wed</h2>
                            <h3 class="degrees">27</h3>
                            <canvas height="32" width="32" id="snow"></canvas>
                            <h5>14 <i>km/h</i></h5>
                          </div>
                        </div>
                        <div class="col-sm-2">
                          <div class="daily-weather">
                            <h2 class="day">Thu</h2>
                            <h3 class="degrees">28</h3>
                            <canvas height="32" width="32" id="sleet"></canvas>
                            <h5>15 <i>km/h</i></h5>
                          </div>
                        </div>
                        <div class="col-sm-2">
                          <div class="daily-weather">
                            <h2 class="day">Fri</h2>
                            <h3 class="degrees">28</h3>
                            <canvas height="32" width="32" id="wind"></canvas>
                            <h5>11 <i>km/h</i></h5>
                          </div>
                        </div>
                        <div class="col-sm-2">
                          <div class="daily-weather">
                            <h2 class="day">Sat</h2>
                            <h3 class="degrees">26</h3>
                            <canvas height="32" width="32" id="cloudy"></canvas>
                            <h5>10 <i>km/h</i></h5>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>

                </div>
                <!-- end of weather widget -->
              </div>
            </div>
          </div>

            <?php require 'js_dashboard.php'; ?>