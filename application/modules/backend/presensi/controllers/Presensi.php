<?php
defined('BASEPATH') OR exit('No direct script access allowed'); require 'application/modules/backend/grab/controllers/Grab.php';

class Presensi extends Grab {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('My_model');
		$this->modul = "presensi";
		$this->tabel = 'presensi';
		$this->id_name = 'id_presensi';
	}

	public function index()
	{
        $this->load->library('pagination');
        $params['v'] = 'search';

        if ($this->input->get('keyword') == true) {
            $params['keyword'] = $this->input->get('keyword');
        }
        if ($this->input->get('range_date') == true) {
            $params['range_date'] = $this->input->get('range_date');
            $range_date = explode(' - ', $params['range_date']);
            // debug($range_date);
        }
        if ($this->input->get('per_page') == true) {
            $params['per_page'] = $this->input->get('per_page');
        }

        $queryString =  http_build_query($params);
        $hpp = explode('&per_page', $queryString);

        $config['base_url'] = base_url().'backend/'.$this->modul.'?'.$hpp[0];
        $config['per_page'] = 5;
        $config['uri_segment'] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        // $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        // $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';        
        $config['last_link'] = 'Last';        
        $config['first_tag_open'] = '<li>';        
        $config['first_tag_close'] = '</li>';        
        $config['prev_link'] = 'Prev';
        $config['prev_tag_open'] = '<li>';      
        $config['prev_tag_close'] = '</li>';          
        $config['next_link'] = 'Next';        
        $config['next_tag_open'] = '<li>';        
        $config['next_tag_close'] = '</li>';        
        $config['last_tag_open'] = '<li>';        
        $config['last_tag_close'] = '</li>';        
        $config['cur_tag_open'] = '<li class="active"><a href="javascript:void()">';        
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';        
        $config['num_tag_close'] = '</li>';

        $offset = 0;
        if (!empty($_GET['per_page'])) {
            $pageNo = $_GET['per_page'];
            $offset = ($pageNo - 1) * $config["per_page"];
        }

        $object = array(
            'limit' => $config['per_page'],
            'offset' => $offset,
            'keyword' => $this->input->get('keyword'),
            'd_r' => isset($range_date) ? $range_date : '',
        );

        $config['total_rows'] = $this->My_model->hitung($object);
        $data['no_urut'] = $offset+1;
        $this->pagination->initialize($config);

        $data['total_rows'] = $this->My_model->hitung($object);
        $data['list_data'] = $this->My_model->getData($object);
        // debug($data['list_data']);

        $data['content'] = "index";
		$this->view($data,false);
	}

	public function form($id=null)
	{
		$data['list_users'] = $this->My_model_main->karyawan();
		$data['list'] = $this->wd_db->get_data_row('presensi', array('id_presensi' => $id, ));
		// debug($data['list_users']);
		$data['content'] = "form";
		$this->view($data);
	}

	public function save()
	{
		$id = $this->input->post('id');
		$id_user = $this->input->post('id_user');
		$tgl_m = $this->input->post('tanggal_masuk');
		$tgl_p = $this->input->post('tanggal_pulang');
		$tgl_p!="" ? $status = '1' : $status = '0';
		$ket_masuk = $this->input->post('keterangan_masuk');
		$ket_pulang = $this->input->post('keterangan_pulang');
		$ob = array(
			'id_user' => $id_user,
			'masuk' => $tgl_m,
			'pulang' => $tgl_p,
			'status_presensi' => $status,
			'keterangan_masuk' => $ket_masuk,
			'keterangan_pulang' => $ket_pulang,
		);
		if ($id) {
			$this->db->where($this->id_name, $id);
			$qi = $this->db->update($this->tabel, $ob);
		}else{
			$qi = $this->db->insert($this->tabel, $ob);
		}
		if ($qi) {
			if ($id) {
				$this->session->set_flashdata('alert_success', 'Data berhasil diedit');
				redirect('backend/'.$this->modul.'/form/'.$id.'');
			}else{
				$this->session->set_flashdata('alert_success', 'Data berhasil ditambahkan');
				redirect('backend/'.$this->modul.'/form');
			}
		}else{
			if ($id) {
				$this->session->set_flashdata('alert_warning', 'Edit data');
				redirect('backend/'.$this->modul.'/form/'.$id.'');
			}else{
				$this->session->set_flashdata('alert_warning', 'Menambah data');
				redirect('backend/'.$this->modul.'/form');
			}
		}
	}

	public function delete($id)
	{
		$this->db->where($this->id_name, $id);
		$qd = $this->db->delete('presensi');
		if ($qd) {
			$this->session->set_flashdata('alert_success', 'Data berhasil dihapus');
			redirect('backend/'.$this->modul);
		}else{
			$this->session->set_flashdata('alert_warning', 'Menghapus data');
			redirect('backend/'.$this->modul);
		}
	}

	public function delete_bulk()
	{
		$id = $this->input->post('table_records');
		if (count($id) < 1) {
			$this->session->set_flashdata('alert_warning', 'Anda belum memilih data');
			redirect('backend/'.$this->modul);
		}
		for ($i=0; $i < count($id); $i++) { 
			$this->db->where($this->id_name, $id[$i]);
			$qd = $this->db->delete($this->tabel);
			if (!$qd) {
				$this->session->set_flashdata('alert_warning', 'Terjadi Kesalahan');
				redirect('backend/'.$this->modul);
			}
		}

		if ($qd) {
			$this->session->set_flashdata('alert_success', 'Data berhasil dihapus');
			redirect('backend/'.$this->modul);
		}
	}

	public function report()
	{
        $params['v'] = 'search';

        if ($this->input->get('keyword') == true) {
            $params['keyword'] = $this->input->get('keyword');
        }
        if ($this->input->get('range_date') == true) {
            $params['range_date'] = $this->input->get('range_date');
            $range_date = explode(' - ', $params['range_date']);
            // debug($aa);
        }
        if ($this->input->get('per_page') == true) {
            $params['per_page'] = $this->input->get('per_page');
        }
        $object = array(
            'limit' => 999999,
            'offset' => 0,
            'keyword' => $this->input->get('keyword'),
            'd_r' => isset($range_date) ? $range_date : '',
        );

        $data['list_data'] = $this->My_model->getData($object);
        $data['d_r'] = isset($range_date) ? $range_date : '';
        // debug($data['d_r']);

		$data['content'] = 'report';
		$this->report_template($data);
	}

	function tutup_presensi0()
	{
		debug($this->My_model->get_presensi_tidak_pulang());
	}

	function tutup_presensi_now()
	{
		$q = $this->My_model->tutup_presensi_now();
		if (count($q) > 0) {
			$this->session->set_flashdata('alert_success', 'Berhasil menutup '.count($q).' presensi');
			redirect('backend/'.$this->modul);
		}else{
			$this->session->set_flashdata('alert_warning', 'Tidak ada user yang tidak presensi pulang');
			redirect('backend/'.$this->modul);
		}
	}

	function tutup_presensi()
	{
		$q = $this->My_model->tutup_presensi();
	}

}

/* End of file Presensi.php */
/* Location: ./application/modules/backend/presensi/controllers/Presensi.php */