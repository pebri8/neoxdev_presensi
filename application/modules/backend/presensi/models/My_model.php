<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_model extends CI_Model {

	var $table = 'presensi';

	public function hitung($object)
	{
    	$this->pagination_join_trip($object);
    	$this->pagination_where_trip($object);
    	$this->pagination_like_trip($object);
        $query = $this->db->get($this->table);
		return $query->num_rows();
	}

	public function getData($object)
	{		
		$this->pagination_like_trip($object);
		$this->pagination_join_trip($object);
		$this->pagination_where_trip($object);
		$this->db->order_by('id_presensi', 'desc');
		return $this->db->get($this->table, $object['limit'], $object['offset'])->result();
	}

	private function pagination_join_trip($object)
	{
		$this->db->join('user', 'user.id_user = presensi.id_user', 'left');
		$this->db->join('bagian', 'bagian.id_bagian = user.id_bagian', 'left');
	}

	private function pagination_where_trip($object)
	{
		if ($object['d_r']) {
			if (isset($object['d_r'][1])) {
				$d_start = date("Y-m-d", strtotime($object['d_r'][0]));
				$d_end = date("Y-m-d", strtotime($object['d_r'][1]));
				$this->db->where("
					masuk >= CAST('".$d_start." 00:00:00' AS DATETIME) AND
					masuk <= CAST('".$d_end." 23:59:59' AS DATETIME)
				");
			}else{
				// debug('hanya satu');
				$d_start = date("Y-m-d", strtotime($object['d_r'][0]));
				$d_end = date("Y-m-d", strtotime($object['d_r'][0]));
				$this->db->where("
					masuk >= CAST('".$d_start." 00:00:00' AS DATETIME) AND
					masuk <= CAST('".$d_end." 23:59:59' AS DATETIME)
				");
			}
		}
	}

	private function pagination_like_trip($object)
	{
		$this->db->where("(
			nama LIKE '%".$object['keyword']."%' OR
			nama LIKE '%".$object['keyword']."%'
			)", NULL, FALSE);
	}

	function get_presensi_tidak_pulang()
	{
		$this->db->where('status_presensi', '0');
		$q = $this->db->get('presensi')->result_array();
		return $q;
	}

	function tutup_presensi()
	{
		$this->db->where('status_presensi', '0');
		$q = $this->db->get('presensi')->result_array();
		foreach ($q as $key => $value) {
			$date_next = date('Y-m-d',strtotime($value['masuk'] . "+1 days"));
			$this->db->where('id_presensi', $value['id_presensi']);
			$ob = array(
				'pulang' => $date_next.' 02:00:00',
				'status_presensi' => '3',
			);
			$q = $this->db->update('presensi', $ob);
		}
		return $q;
	}

	function tutup_presensi_now()
	{
		$this->db->where('status_presensi', '0');
		$q = $this->db->get('presensi')->result_array();
		foreach ($q as $key => $value) {
			$date_next = date('Y-m-d',strtotime($value['masuk'] . "+1 days"));
			$this->db->where('id_presensi', $value['id_presensi']);
			$ob = array(
				'pulang' => date('Y-m-d H:i:s'),
				'status_presensi' => '3',
			);
			$qu = $this->db->update('presensi', $ob);
		}
		return $q;
	}

}

/* End of file My_model.php */
/* Location: ./application/modules/backend/presensi/models/My_model.php */