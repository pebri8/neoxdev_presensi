            <div class="page-title">
              <div class="title_left">
                <h3>Presensi / Form</h3>
              </div>

            </div>

            <div class="clearfix"></div>

            <?php require_once __DIR__."/../../blocks/alert_notification.php"; ?>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Form Tambah Presensi <small>Silahkan diisi</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form action="<?php echo base_url() ?>backend/<?php echo $this->modul ?>/save" method="post" id="form" data-parsley-validate class="form-horizontal form-label-left">
                    <input type="hidden" name="id" value="<?php echo $list ? $list['id_presensi'] : '' ?>">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
            							<select required id="select" name="id_user" class="form-control col-md-6 col-sm-6 col-xs-12" style="width: 100%;">
            							  <option disabled selected value="AL">Pilih Karyawan</option>
            							  <?php foreach ($list_users as $dt): ?>
            							  	<option <?php echo ($list ? $list['id_user'] : '') == $dt['id_user'] ? ' selected' : '' ?> value="<?php echo $dt['id_user'] ?>"><?php echo $dt['nama']; ?></option>
            							  <?php endforeach ?>
            							</select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Masuk <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" name="tanggal_masuk" required value="<?php echo $list ? $list['masuk'] : '' ?>" 
								onkeydown="return false"
								class="datepicker-here form-control col-md-7 col-xs-12"
								data-language='en'
								data-min-view="days"
								data-view="days"
								data-date-format="yyyy-mm-dd"
								data-timepicker="true" 
								data-time-format='hh:ii:00' />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Pulang <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" name="tanggal_pulang" required value="<?php echo $list ? $list['pulang'] : '' ?>" 
								onkeydown="return false"
								class="datepicker-here form-control col-md-7 col-xs-12"
								data-language='en'
								data-min-view="days"
								data-view="days"
								data-date-format="yyyy-mm-dd"
								data-timepicker="true" 
								data-time-format='hh:ii:00' />
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Keterangan Masuk <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea name="keterangan_masuk" class="form-control"><?php echo $list ? $list['keterangan_masuk'] : '' ?></textarea>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Keterangan Pulang <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea name="keterangan_pulang" class="form-control"><?php echo $list ? $list['keterangan_pulang'] : '' ?></textarea>
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="submit" class="btn btn-success">Simpan</button>
						  <button class="btn btn-primary" type="reset">Reset</button>
                          <a class="btn btn-primary" href="<?php echo base_url() ?>backend/<?php echo $this->modul ?>">Kembali</a>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>

	<script>
	$('#ddd').datepicker({
	    range: 'true',
	    language: 'en',
	    maxDate: new Date()
	});
	</script>

	<script>
	$(document).ready(function() {
	    $('#select').select2();
	});
	</script>