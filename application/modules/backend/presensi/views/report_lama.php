		<table width="800" align="center" border="0" cellpadding="0" cellspacing="0">
			<tr><th colspan="3" class="tl">LAPORAN PRESENSI</th></tr>
			<tr>
				<td width="90">Dari bulan</td><td width="12">:</td>
				<td><?php echo isset($d_r[0]) ? $d_r[0] : '-' ; ?></td>
			</tr>
			<tr>
				<td>Sampai</td><td>:</td>
				<td><?php echo isset($d_r[1]) ? $d_r[1] : '-' ; ?></td>
			</tr>
		</table>
        <br>
        <table width="800" align="center" border="0" cellpadding="0" cellspacing="0" class="krs_box">
            <tbody>
                <tr>
                    <th width="5%">NO</th>
                    <th width="20%">NAMA</th>
                    <th width="15%">MASUK</th>
                    <th width="15%">PULANG</th>
                    <th width="15%">STATUS PRESENSI</th>
                    <th width="15%">KET MASUK</th>
                    <th width="15%">KET PULANG</th>
                </tr>
                <?php $selislih = 0; ?>
                <?php $no = 1; ?>
                <?php foreach ($list_data as $dt): ?>
                <tr>
                    <td><?php echo $no; $no++; ?></td>
                    <td class="td"><?php echo $dt->nama ?></td>
                    <td><?php echo $dt->masuk ?></td>
                    <td><?php echo $dt->pulang ?></td>
                    <td><?php echo $dt->status_presensi == '1' ? 'Complete' : 'Jam pulang kosong' ?></td>
                    <td class="td"><?php echo $dt->keterangan_masuk ?></td>
                    <td class="td"><?php echo $dt->keterangan_masuk ?></td>

                    <!-- sum hours -->
                    <?php 
                        $h_masuk = explode(' ', $dt->masuk);
                        $h_masuk = explode(':', $h_masuk[1]);
                        $hr_m_fix = ($h_masuk[0]*60)+$h_masuk[1];

                        $h_pulang = explode(' ', $dt->pulang);
                        $h_pulang = explode(':', $h_pulang[1]);
                        $hr_p_fix = ($h_pulang[0]*60)+$h_pulang[1];

                        $selislih = $selislih + $hr_p_fix - $hr_m_fix;

                        $t_hr = $selislih;
                        $selisih_fix = floor($t_hr/60)." hr ".($t_hr%60)." min";
                    ?>
                </tr>
                <?php endforeach ?>
                <tr>
                    <td class="tr" colspan="4"><strong>Total Jam Kerja</strong></td>
                    <td class="td" colspan="3"><strong><?php echo $selisih_fix; ?></strong></td>
                </tr>
            </tbody>
        </table>