		<table width="800" align="center" border="0" cellpadding="0" cellspacing="0">
			<tr><th colspan="3" class="tl">LAPORAN PRESENSI</th></tr>
			<tr>
				<td width="90">Dari bulan</td><td width="12">:</td>
				<td><?php echo isset($d_r[0]) ? $d_r[0] : '-' ; ?></td>
			</tr>
			<tr>
				<td>Sampai</td><td>:</td>
				<td><?php echo isset($d_r[1]) ? $d_r[1] : '-' ; ?></td>
			</tr>
		</table>
        <br>
        <table width="800" align="center" border="0" cellpadding="0" cellspacing="0" class="krs_box">
            <tbody>
                <tr>
                    <th width="4%">NO</th>
                    <th width="15%">NAMA</th>
                    <th width="15%">MASUK</th>
                    <th width="15%">PULANG</th>
                    <th width="8%">SELISIH</th>
                    <th width="13%">STATUS</th>
                    <th width="15%">KET MASUK</th>
                    <th width="15%">KET PULANG</th>
                </tr>
                <?php $selisih = 0; ?>
                <?php $no = 1; ?>
                <?php echo $beda = false; ?>
                <?php foreach ($list_data as $dt): ?>
                    <?php 
                        if ($no==1) {
                            $id_user_awal = $dt->id_user;
                        }$no++;

                        // jika ada data yang beda (bukan 1 user/pegawai) $beda=true
                        if ($dt->id_user == $id_user_awal) {
                        }else{
                            $beda = true;
                        }
                    ?>
                <?php endforeach ?>

                <?php $no = 1; ?>
                <?php if ($beda == true): ?>
                    <?php foreach ($list_data as $dt): ?>
                        <tr>
                            <td><?php echo $no; $no++; ?></td>
                            <td class="td"><?php echo $dt->nama ?></td>
                            <td><?php echo $dt->masuk ?></td>
                            <td><?php echo $dt->pulang ?></td>

<!-- sum hours -->
<?php 
    // new
    $awal  = date_create($dt->masuk);
    $akhir = date_create($dt->pulang);
    $diff  = date_diff( $awal, $akhir );
    $selislih_per_presensi = ($diff->d*24)+($diff->h).' hr '.$diff->i.' min ';

    $selisih = ($diff->days*24*60)+($diff->h*60)+($diff->i)+($selisih);
    $selisih_fix = floor($selisih/60)." hr ".($selisih%60)." min";
?>
                          
                            <td><?php echo $selislih_per_presensi ?></td>
                            <td><?php echo $dt->status_presensi == '1' ? 'Complete' : 'Jam pulang kosong' ?></td>
                            <td class="td"><?php echo $dt->keterangan_masuk ?></td>
                            <td class="td"><?php echo $dt->keterangan_masuk ?></td>
                        </tr>
                    <?php endforeach ?>
                <?php else: ?>
                    <?php $jml_presensi=0; ?>
                    <?php foreach ($list_data as $dt): ?>
                        <tr>
<!-- cek apakah dalam sehari lebih dari sekali presensi -->
<?php 
    if ($jml_presensi == 0) {
        $tanggal_presensi = date("Y-m-d", strtotime($dt->masuk));
        $id_user = $dt->id_user;
        $this->db->where("(
        masuk LIKE '%".$tanggal_presensi."%' AND
        id_user LIKE '%".$id_user."%'
        )", NULL, FALSE);
        $list_p = $this->db->get('presensi')->result_array();
        $jml_presensi_awal = count($list_p);
        $jml_presensi = $jml_presensi_awal;
        if ($jml_presensi_awal > 1) {
            $gabung = true;
        }else{
            $gabung = false;
        }
    }

    if ($gabung==true) {
        if ($jml_presensi == $jml_presensi_awal) {
            $gabung_v = 'rowspan="'.$jml_presensi.'"';
        }else{
            $gabung_v = 'style="display:none"';
        }
    }else{
        $gabung_v = '';
    }

?>

                            <td <?php echo $gabung_v; ?>><?php echo $no; ?></td>
                            <td <?php echo $gabung_v; ?> class="td"><?php echo $dt->nama ?></td>
<?php
    $jml_presensi--;
    if ($jml_presensi < 1) {
        $no++;
    }
?>
                            <td><?php echo $dt->masuk ?></td>
                            <td><?php echo $dt->pulang ?></td>

<!-- sum hours -->
<?php 
    $h_masuk = explode(' ', $dt->masuk);
    $h_masuk = explode(':', $h_masuk[1]);
    $hr_m_fix = ($h_masuk[0]*60)+$h_masuk[1];

    $h_pulang = explode(' ', $dt->pulang);
    $h_pulang = explode(':', $h_pulang[1]);
    $hr_p_fix = ($h_pulang[0]*60)+$h_pulang[1];

    $selislih_per_presensi = $hr_p_fix - $hr_m_fix;
    $selislih_per_presensi = floor($selislih_per_presensi/60)." hr ".($selislih_per_presensi%60)." min";

    // new
    $awal  = date_create($dt->masuk);
    $akhir = date_create($dt->pulang);
    $diff  = date_diff( $awal, $akhir );
    $selislih_per_presensi = ($diff->d*24)+($diff->h).' hr '.$diff->i.' min ';

    $selisih = ($diff->days*24*60)+($diff->h*60)+($diff->i)+($selisih);
    $selisih_fix = floor($selisih/60)." hr ".($selisih%60)." min";
?>
                          
                            <td><?php echo $selislih_per_presensi ?></td>
                            <td><?php echo $dt->status_presensi == '1' ? 'Complete' : 'Jam pulang kosong' ?></td>
                            <td class="td"><?php echo $dt->keterangan_masuk ?></td>
                            <td class="td"><?php echo $dt->keterangan_masuk ?></td>

                        </tr>
                    <?php endforeach ?>
                <?php endif ?>
                <tr>
                    <td class="tr" colspan="4"><strong>Total Jam Kerja</strong></td>
                    <td class="td" colspan="4"><strong><?php echo $selisih_fix; ?></strong></td>
                </tr>
            </tbody>
        </table>