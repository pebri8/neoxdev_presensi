<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo base_url() ?>assets/img/logo_neox.svg" type="image/ico/svg" />

    <title>Presensi Admin | Feeltrip.org </title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url() ?>assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url() ?>assets/admin/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url() ?>assets/admin/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo base_url() ?>assets/admin/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url() ?>assets/admin/build/css/custom.min.css" rel="stylesheet">

    <link href="<?php echo base_url() ?>assets/admin/build/css/custom2.css" rel="stylesheet">

    <!-- jQuery -->
    <script src="<?php echo base_url() ?>assets/admin/vendors/jquery/dist/jquery.min.js"></script>

    <!-- Jquery Confirm -->
    <link href="<?php echo base_url() ?>assets/jconfirm/jquery-confirm.min.css" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url() ?>assets/jconfirm/jquery-confirm.min.js"></script>

    <!-- Form -->

    <!-- Air Datepicker -->
    <link href="<?php echo base_url() ?>assets/admin/air-datepicker/dist/css/datepicker.min.css" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url() ?>assets/admin/air-datepicker/dist/js/datepicker.min.js"></script>

    <!-- Include English language -->
    <script src="<?php echo base_url() ?>assets/admin/air-datepicker/dist/js/i18n/datepicker.en.js"></script>

    <!-- Select2 -->
    <link href="<?php echo base_url() ?>assets/select2/select2.min.css" rel="stylesheet" />
    <script src="<?php echo base_url() ?>assets/select2/select2.min.js"></script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
            <a href="index.html" class="site_title">
                <span>
                    <img style="height: 40px; padding: 0px;" src="<?php echo base_url() ?>assets/img/logo_neox.svg" alt="NeoxDev"> 
                </span>
            </a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
              <?php require_once __DIR__."/../../blocks/sidebar_menu_profile.php"; ?>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
              <?php require_once __DIR__."/../../blocks/sidebar_menu.php"; ?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
              <?php require_once __DIR__."/../../blocks/sidebar_menu_footer.php"; ?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
          <?php require_once __DIR__."/../../blocks/top_navigation.php"; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <?php if(isset($content)) $this->load->view($content); ?>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php require_once __DIR__."/../../blocks/footer.php"; ?>
        <!-- /footer content -->
      </div>
    </div>


    <!-- Bootstrap -->
    <script src="<?php echo base_url() ?>assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() ?>assets/admin/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url() ?>assets/admin/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url() ?>assets/admin/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="<?php echo base_url() ?>assets/admin/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    
    <!-- Skycons -->
    <script src="<?php echo base_url() ?>assets/admin/vendors/skycons/skycons.js"></script>
    <!-- JQVMap -->
    <script src="<?php echo base_url() ?>assets/admin/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?php echo base_url() ?>assets/admin/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?php echo base_url() ?>assets/admin/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>

    <!-- Flot -->  
    <script src="<?php echo base_url() ?>assets/admin/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url() ?>assets/admin/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url() ?>assets/admin/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url() ?>assets/admin/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url() ?>assets/admin/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url() ?>assets/admin/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url() ?>assets/admin/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url() ?>assets/admin/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url() ?>assets/admin/vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url() ?>assets/admin/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/admin/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url() ?>assets/admin/build/js/custom.min.js"></script>

  </body>
</html>